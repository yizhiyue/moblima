package Helper;

import java.util.*;
/**
 * A helper class to read input from console with error handling.
 */
public class InputHelper {
	private static Scanner sc = new Scanner(System.in);

	/**
	 * Gets the integer value from input.
	 * 
	 * @return the input value of data type integer
	 */
	public static int getInt() {
		int num = 0;
		boolean invalid = true;

		do {
			try {
				num = sc.nextInt();
				invalid = false;
			} catch (Exception exc) {
				sc.nextLine();
				System.out.print("Enter only numbers!\nInput : ");
			}
		} while (invalid);
		sc.nextLine();
		return num;
	}

	/**
	 * Gets the double value from input.
	 * 
	 * @return the input value of data type double
	 */
	public static double getDouble() {
		double num = 0;
		boolean invalid = true;

		do {
			try {
				num = sc.nextDouble();
				invalid = false;
			} catch (Exception exc) {
				sc.nextLine();
				System.out.print("Enter only numbers!\nInput : ");
			}
		} while (invalid);
		sc.nextLine();
		return num;
	}

	/**
	 * Gets the long value from input.
	 * 
	 * @return the input value of data type long
	 */
	public static long getLong() {
		long num = 0;
		boolean invalid = true;

		do {
			try {
				num = sc.nextLong();
				invalid = false;
			} catch (Exception exc) {
				sc.nextLine();
				System.out.print("Enter only numbers!\nInput : ");
			}
		} while (invalid);
		sc.nextLine();
		return num;
	}

	/**
	 * Gets the boolean value from input.
	 * 
	 * @return the boolean value of data type integer
	 */
	public static boolean getBoolean() {
		boolean bool = false;
		boolean invalid = true;

		do {
			try {
				bool = sc.nextBoolean();
				invalid = false;
			} catch (Exception exc) {
				sc.nextLine();
				System.out.print("Enter only numbers!\nInput : ");
			}
		} while (invalid);
		return bool;
	}
	
	/**
	 * Gets the String for one word value from input.
	 * 
	 * @return the input value of data type String for one word
	 */
	public static String getWord() {
		String str = sc.next();
		sc.nextLine();
		return str;
	}

	/**
	 * Gets the String value from input.
	 * 
	 * @return the input value of data type String
	 */
	public static String getString() {
		String str = sc.nextLine();
		return str;
	}
	
	/**
	 * Gets the int values from input and construct a Calendar object.
	 * 
	 * @return the Calendar object consisted of input values
	 */
	public static Calendar getDateTime() {
		Calendar date = Calendar.getInstance();
		int year = 0;
		int month = 0;
		int day = 0;
		int hour = 0;
		int minute = 0;
		boolean invalid = true;
		
		do {
			try {
				System.out.print("Year: ");
				year = sc.nextInt();
				System.out.print("Month: ");
				month = sc.nextInt();
				System.out.print("Day: ");
				day = sc.nextInt();
				System.out.print("Hour: ");
				hour = sc.nextInt();
				System.out.print("Minute: ");
				minute = sc.nextInt();
				date.set(Calendar.YEAR, year);
				date.set(Calendar.MONTH, month - 1);
				date.set(Calendar.DATE, day);
				date.set(Calendar.HOUR_OF_DAY, hour);
				date.set(Calendar.MINUTE, minute);
				invalid = false;
			} catch (Exception exc) {
				sc.nextLine();
				System.out.print("Enter only number!\nInput : ");
			}
		} while (invalid);
		sc.nextLine();
		
		return date;
	}
	
	/**
	 * Gets the int values from input and construct a Calendar object with only date, month and year.
	 * 
	 * @return the Calendar object consisted of input values
	 */
	public static Calendar getDate() {
		Calendar date = Calendar.getInstance();
		int year = 0;
		int month = 0;
		int day = 0;
		int hour = 0;
		int minute = 0;
		boolean invalid = true;
		
		do {
			try {
				System.out.print("Year: ");
				year = sc.nextInt();
				System.out.print("Month: ");
				month = sc.nextInt();
				System.out.print("Day: ");
				day = sc.nextInt();
				date.set(Calendar.YEAR, year);
				date.set(Calendar.MONTH, month - 1);
				date.set(Calendar.DATE, day);
				date.set(Calendar.HOUR_OF_DAY, hour);
				date.set(Calendar.MINUTE, minute);
				invalid = false;
			} catch (Exception exc) {
				sc.nextLine();
				System.out.print("Enter only number!\nInput : ");
			}
		} while (invalid);
		sc.nextLine();
		
		return date;
	}
}
