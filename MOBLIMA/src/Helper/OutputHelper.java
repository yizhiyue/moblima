package Helper;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import Entity.*;

/**
 * A helper class to handle output in console.
 */
public class OutputHelper {
	private final static int MAX_WIDTH = 150;
	private final static String STR_MOVIE_TABLE_FORMAT = "%-15s%-40s%-30s%-15s%-15s%-20s%-20s%-100s\n";
	private final static String STR_MOVIE_TABLE_FORMAT_WITH_INDEX = "%-6s%-40s%-30s%-15s%-15s%-20s%-20s%-100s\n";
	private final static String STR_MOVIE_TABLE_FORMAT_RANKING = "%-40s%-6s\n";
	private final static String STR_TABLE_SYNOPSIS_FORMAT = "%-15s%s\n";
	private final static String STR_MOVIE_TYPE_TABLE_FORMAT = "%-15s%-15s%-15s\n";
	private final static String STR_CINEPLEX_TABLE_FORMAT = "%-6s%-20s%-15s%-15s%-100s\n";
	private final static String STR_CINEMA_TABLE_FORMAT = "%-6s%-20s%-15s%-15s%-15s\n";
	private final static String STR_SCHEDULE_TABLE_FORMAT = "%-15s%-20s%-15s%-40s%-20s%-20s\n";
	private final static String STR_SCHEDULE_TABLE_FORMAT_WITH_INDEX = "%-6s%-20s%-15s%-40s%-20s%-20s\n";
	private final static String STR_BOOKING_TABLE_FORMAT = "%-20s%-20s%-15s%-40s%-20s%-20s%-20s%-40s\n";
	private final static String STR_FEEDBACK_TABLE_FORMAT = "%-10s%-50s\n";

	// Add new STR_HOLIDAY_TABLE_FORMAT and STR_HOLIDAY_TABLE_FORMAT_WITH_INDEX

	private final static String STR_HOLIDAY_TABLE_FORMAT = "%-15s%-15s%-15s\n";

	/**
	 * Construct display pattern long text into wrapped text
	 * 
	 * @param longString
	 *            the target long string to be printed
	 * @return the wrapped text
	 */
	public static String wrapText(String longString) {
		String[] splittedString = longString.split(" ");
		String resultString = "";
		String lineString = "";

		for (int i = 0; i < splittedString.length; i++) {
			if (lineString.isEmpty()) {
				lineString += splittedString[i] + " ";
			} else if (lineString.length() + splittedString[i].length() < MAX_WIDTH) {
				lineString += splittedString[i] + " ";
			} else {
				resultString += lineString + "\n";
				lineString = "";
			}
		}

		if (!lineString.isEmpty()) {
			resultString += lineString + "\n";
		}

		return resultString;
	}

	/**
	 * Construct display pattern of movie type list
	 * 
	 * @param movieTypeList
	 *            movie type list in array form to be printed
	 */
	public static void printMovieTypeList(Movie.MovieType[] movieTypeList) {
		System.out.format(STR_MOVIE_TYPE_TABLE_FORMAT, "Index", "Name", "Additional Price");
		for (int i = 0; i < movieTypeList.length; i++) {
			System.out.format(STR_MOVIE_TYPE_TABLE_FORMAT, (i + 1) + ".", movieTypeList[i],
					"$" + get2DecimalPlacesString(Movie.getAdditionalTicketPrice(movieTypeList[i])));
		}
	}

	/**
	 * Construct display pattern of movie list
	 * 
	 * @param movies
	 *            target movie list to be printed
	 * @param isShowIndex
	 *            boolean value to define whether to show index
	 */
	public static void printMovieList(List<Movie> movies, boolean isShowIndex) {
		if (isShowIndex) {
			System.out.format(STR_MOVIE_TABLE_FORMAT_WITH_INDEX, "ID", "Title", "Type", "Avg Score", "Rating", "Status",
					"Duration", "Director", "Casts");
		} else {
			System.out.format(STR_MOVIE_TABLE_FORMAT, "ID", "Title", "Type", "Avg Score", "Rating", "Status",
					"Duration", "Director", "Casts");
		}

		for (int i = 0; i < movies.size(); i++) {
			Movie movie = movies.get(i);
			printMovieItem(movie, i, false, isShowIndex);
		}
	}

	/**
	 * Construct display pattern of holiday list
	 * 
	 * @param holidays
	 *            target holiday list to be printed
	 */
	public static void printHolidayList(List<Holiday> holidays) {

		System.out.format(STR_HOLIDAY_TABLE_FORMAT, "ID", "Name", "Date", "Month", "Year");

		for (int i = 0; i < holidays.size(); i++) {
			System.out.format(STR_HOLIDAY_TABLE_FORMAT, holidays.get(i).getId(), holidays.get(i).getName(),
					getDateDisplayString(holidays.get(i).getDate()));

		}
	}

	/**
	 * Construct display pattern of movie item
	 * 
	 * @param movie
	 *            target movie item to be printed
	 * @param index
	 *            if user chooses to show index, this is the index value
	 * @param isShowDetail
	 *            boolean value to define whether to show movie details including
	 *            synopsis, reviews and ratings
	 * @param isShowIndex
	 *            boolean value to define whether to show index
	 */
	public static void printMovieItem(Movie movie, int index, boolean isShowDetail, boolean isShowIndex) {
		// Construct a string of all casts
		String[] casts = movie.getCasts();

		String castsStr = "";
		for (int j = 0; j < casts.length; j++) {
			castsStr += casts[j];
			if (j != (casts.length - 1))
				castsStr += ", ";
		}

		String movieTypeStr = "";
		Movie.MovieType[] movieTypes = movie.getMovieType();
		for (int k = 0; k < movieTypes.length; k++) {
			movieTypeStr += movieTypes[k];
			if (k != (movieTypes.length - 1))
				movieTypeStr += ", ";
		}

		double totalPoints = 0;
		for (int j = 0; j < movie.getFeedback().size(); j++) {
			totalPoints += movie.getFeedback().get(j).getPoints();
		}

		String customerRating = movie.getFeedback().size() > 1
				? get2DecimalPlacesString(totalPoints / movie.getFeedback().size())
				: "NA";

		if (isShowIndex) {
			System.out.format(STR_MOVIE_TABLE_FORMAT_WITH_INDEX, index + 1, movie.getTitle(), movieTypeStr,
					customerRating, movie.getMovieRating(), movie.getStatus(), movie.getDuration() + " mins",
					movie.getDirector(), castsStr);
		} else {
			System.out.format(STR_MOVIE_TABLE_FORMAT, movie.getId(), movie.getTitle(), movieTypeStr, customerRating,
					movie.getMovieRating(), movie.getStatus(), movie.getDuration() + " mins", movie.getDirector(),
					castsStr);
		}

		if (isShowDetail) {
			System.out.println("Synopsis: ");
			System.out.println(OutputHelper.wrapText(movie.getSynopsis()));
			System.out.format(STR_FEEDBACK_TABLE_FORMAT, "Ratings", "Reveiws");
			ArrayList<Feedback> feedbacks = movie.getFeedback();
			for (int i = 0; i < feedbacks.size(); i++) {
				System.out.format(STR_FEEDBACK_TABLE_FORMAT, feedbacks.get(i).getPoints(),
						feedbacks.get(i).getReview());
			}
		}
	}

	/**
	 * Construct display pattern of ranked movies list by ratings
	 * 
	 * @param movies
	 *            target movies list to be printed
	 */
	public static void printRankedByRatingMovieList(List<Movie> movies) {

		System.out.format(STR_MOVIE_TABLE_FORMAT_RANKING, "Title", "Average Rating");

		for (int i = 0; i < movies.size(); i++) {
			Movie movie = movies.get(i);
			double totalPoints = 0;
			for (int j = 0; j < movie.getFeedback().size(); j++) {
				totalPoints += movie.getFeedback().get(j).getPoints();
			}

			String customerRating = movie.getFeedback().size() > 1
					? get2DecimalPlacesString(totalPoints / movie.getFeedback().size())
					: "NA";

			System.out.format(STR_MOVIE_TABLE_FORMAT_RANKING, movie.getTitle(), customerRating);
		}
	}

	/**
	 * Construct display pattern of ranked movies list by ticket sold
	 * 
	 * @param movies
	 *            target movies list to be printed
	 */
	public static void printRankedByTicketsSoldMovieList(List<Movie> movies) {

		System.out.format(STR_MOVIE_TABLE_FORMAT_RANKING, "Title", "No. of Ticket Sold");

		for (int i = 0; i < movies.size(); i++) {
			Movie movie = movies.get(i);
			System.out.format(STR_MOVIE_TABLE_FORMAT_RANKING, movie.getTitle(), movie.getNoOfTicketSold());
		}
	}

	/**
	 * Construct display pattern of cinema list
	 * 
	 * @param cinemas
	 *            target cinema list to be printed
	 */
	public static void printCinemaList(List<Cinema> cinemas) {
		System.out.format(STR_CINEMA_TABLE_FORMAT, "Index", "Name", "Short Code", "Class", "No. of Seat");
		for (int i = 0; i < cinemas.size(); i++) {
			System.out.format(STR_CINEPLEX_TABLE_FORMAT, (i + 1) + ".", cinemas.get(i).getName(),
					cinemas.get(i).getShortCode(), cinemas.get(i).getCinemaClass().name(),
					cinemas.get(i).getTotalSeat());
		}
	}

	/**
	 * Construct display pattern of cineplex list
	 * 
	 * @param cineplexes
	 *            target cineplex list to be printed
	 */
	public static void printCineplexList(List<Cineplex> cineplexes) {
		System.out.format(STR_CINEPLEX_TABLE_FORMAT, "Index", "Name", "Short Code", "Contact", "Address");
		for (int i = 0; i < cineplexes.size(); i++) {
			System.out.format(STR_CINEPLEX_TABLE_FORMAT, (i + 1) + ".", cineplexes.get(i).getName(),
					cineplexes.get(i).getShortCode(), cineplexes.get(i).getContact(), cineplexes.get(i).getAddress());
		}
	}

	/**
	 * Construct display pattern of schedule list
	 * 
	 * @param schedules
	 *            target schedule list to be printed
	 * @param isShowIndex
	 *            boolean value to define whether to show index
	 */
	public static void printScheduleList(List<Schedule> schedules, boolean isShowIndex) {
		if (isShowIndex)
			System.out.format(STR_SCHEDULE_TABLE_FORMAT_WITH_INDEX, "Index", "Cineplex", "Cinema", "Movie",
					"Start Date Time", "End Date Time");
		else
			System.out.format(STR_SCHEDULE_TABLE_FORMAT, "Schedule ID", "Cineplex", "Cinema", "Movie",
					"Start Date Time", "End Date Time");
		for (int i = 0; i < schedules.size(); i++) {
			Schedule schedule = schedules.get(i);
			OutputHelper.printScheduleItem(schedule, isShowIndex, i);
		}
	}

	/**
	 * Construct display pattern of schedule item
	 * 
	 * @param schedule
	 *            target schedule to be printed
	 * @param isShowIndex
	 *            boolean value to define whether to show index
	 * @param index
	 *            the actual index number
	 */
	public static void printScheduleItem(Schedule schedule, boolean isShowIndex, int index) {
		Calendar startDatetime = schedule.getShowingDateTime();
		Calendar endDatetime = getEndTime(schedule.getShowingDateTime(), schedule.getMovie());
		if (isShowIndex)
			System.out.format(STR_SCHEDULE_TABLE_FORMAT_WITH_INDEX, (index + 1),
					schedule.getCinema().getCineplex().getName(), schedule.getCinema().getName(),
					schedule.getMovie().getTitle(), getDateTimeDisplayString(startDatetime),
					getDateTimeDisplayString(endDatetime));
		else
			System.out.format(STR_SCHEDULE_TABLE_FORMAT, schedule.getId(), schedule.getCinema().getCineplex().getName(),
					schedule.getCinema().getName(), schedule.getMovie().getTitle(),
					getDateTimeDisplayString(startDatetime), getDateTimeDisplayString(endDatetime));
	}

	/**
	 * calculate the end time of a movie
	 * 
	 * @param startDateTime
	 *            start date time of a movie
	 * @param movie
	 *            target movie to process
	 * @return calculated end time of the target movie
	 */
	public static Calendar getEndTime(Calendar startDateTime, Movie movie) {
		Calendar endDateTime = Calendar.getInstance();
		endDateTime.set(Calendar.YEAR, startDateTime.get(Calendar.YEAR));
		endDateTime.set(Calendar.MONTH, startDateTime.get(Calendar.MONTH));
		endDateTime.set(Calendar.DATE, startDateTime.get(Calendar.DATE));
		endDateTime.set(Calendar.HOUR_OF_DAY, startDateTime.get(Calendar.HOUR_OF_DAY));
		endDateTime.set(Calendar.MINUTE, startDateTime.get(Calendar.MINUTE) + movie.getDuration());
		return endDateTime;
	}

	/**
	 * Construct display pattern of calendar datetime object
	 * 
	 * @param datetime
	 *            target calendar datetime object
	 * @return the date time format dd-MM-yyyy kk:mm of a Calendar object
	 */
	public static String getDateTimeDisplayString(Calendar datetime) {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy kk:mm");
		return format.format(datetime.getTime());
	}

	/**
	 * Construct display pattern of calendar date object
	 * 
	 * @param datetime
	 *            target calendar datetime object
	 * @return the date time format dd-MM-yyyy of a Calendar object
	 */
	public static String getDateDisplayString(Calendar datetime) {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		return format.format(datetime.getTime());
	}

	/**
	 * Construct display pattern of seat arrangement
	 * 
	 * @param seats
	 *            array of seats
	 */
	public static void printSeatArrangement(String[][] seats) {
		int midCol = seats[0].length / 2;

		System.out.print("    ");
		for (int j = 0; j < seats[0].length; j++) {
			System.out.print((j + 1) + " ");
			if (j == midCol)
				System.out.print("  ");
		}
		System.out.println();
		for (int i = 0; i < seats.length; i++) {
			System.out.print((i + 1) + "   ");
			for (int j = 0; j < seats[i].length; j++) {
				switch (seats[i][j]) {
				case "Available":
					System.out.print("A" + " ");
					break;
				case "Taken":
					System.out.print("X" + " ");
				}
				if (j == midCol)
					System.out.print("  ");
			}

			System.out.println();
		}
	}

	/**
	 * Construct display pattern of bookings
	 * 
	 * @param bookings
	 *            list of bookings
	 */
	public static void printBookingList(List<Booking> bookings) {
		System.out.format(STR_BOOKING_TABLE_FORMAT, "Transaction ID", "Cineplex", "Cinema", "Movie", "Start Date Time",
				"End Date Time", "Price", "Seat(s)");
		for (int i = 0; i < bookings.size(); i++) {
			String seatsString = "";
			for (int j = 0; j < bookings.get(i).getSeats().length; j++) {
				seatsString += "Row " + bookings.get(i).getSeats()[j].getCol() + " Col "
						+ bookings.get(i).getSeats()[j].getRow();
				if (j != bookings.get(i).getSeats().length - 1)
					seatsString += ", ";
			}

			System.out.format(STR_BOOKING_TABLE_FORMAT, bookings.get(i).getTransactionId(),
					bookings.get(i).getSchedule().getCinema().getCineplex().getName(),
					bookings.get(i).getSchedule().getCinema().getName(),
					bookings.get(i).getSchedule().getMovie().getTitle(),
					getDateTimeDisplayString(bookings.get(i).getSchedule().getShowingDateTime()),
					getDateTimeDisplayString(getEndTime(bookings.get(i).getSchedule().getShowingDateTime(),
							bookings.get(i).getSchedule().getMovie())),
					"$" + get2DecimalPlacesString(bookings.get(i).getTransactionAmount()), seatsString);
		}
	}

	/**
	 * return round a double to 2 decimal places in string
	 * 
	 * @param number
	 *            the double value to be processed
	 * @return the string of a double in 2 decimal places
	 */
	public static String get2DecimalPlacesString(Double number) {
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		return df.format(number);
	}
}
