package Dao;

import java.util.*;
import Database.SerializeDB;
import Entity.Movie;

public class MovieDAOImpl implements MovieDAO {
	private static final String DBPATH = "data/dbMovie.dat";
	private List<Movie> movies;

	public MovieDAOImpl() {

	}

	/**
	 * Read all movies from data file.
	 */
	public void openDb() {
		List<Movie> output = SerializeDB.readSerializedObject(DBPATH);
		movies = output != null ? output : new ArrayList<Movie>();
	}

	/**
	 * Save all movies from data file.
	 */
	public void saveDb() {
		SerializeDB.writeSerializedObject(DBPATH, movies);
	}

	/**
	 * Returns a list of all movies.
	 * 
	 * @return a list of all movies.
	 */
	@Override
	public List<Movie> readAll() {
		try {
			openDb();
			return movies;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Returns the target movie.
	 * 
	 * @return the target movie.
	 */
	@Override
	public Movie read(long id) {
		try {
			openDb();
			Movie movie = null;
			for (int i = 0; i < movies.size(); i++) {
				if (movies.get(i).getId() == id) {
					movie = movies.get(i);
					break;
				}
			}
			return movie;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Update a movie record.
	 */
	@Override
	public boolean update(long id, Movie movie) {
		try {
			openDb();
			int i;
			for (i = 0; i < movies.size(); i++) {
				if (movies.get(i).getId() == id) {
					movies.set(i, movie);
					break;
				}
			}
			if (i == movies.size())
				throw new Exception("Movie not found.");
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Create a movie record.
	 */
	@Override
	public boolean create(Movie movie) {
		try {
			openDb();
			movies.add(movie);
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Delete a movie record.
	 */
	@Override
	public boolean delete(long id) {
		try {
			openDb();
			int i;
			for (i = 0; i < movies.size(); i++) {
				if (movies.get(i).getId() == id)
					break;
			}
			if (i == movies.size())
				throw new Exception("Movie not found.");
			movies.remove(i);
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Delete all movie records.
	 * 
	 * @return result of success or failure of deleting all object
	 */
	@Override
	public boolean deleteAll() {
		try {
			openDb();
			movies.clear();
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Read movies by title keyword
	 * 
	 * @param keyword
	 *            user input keyword for movies
	 * @return movies found based on keyword
	 */
	public List<Movie> readByTitle(String keyword) {
		try {
			openDb();
			List<Movie> moviesFound = new ArrayList<Movie>();
			for (int i = 0; i < movies.size(); i++) {
				if (movies.get(i).getTitle().toLowerCase().contains(keyword.toLowerCase())) {
					moviesFound.add(movies.get(i));
				}
			}
			return moviesFound;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Read movies by title keyword
	 * 
	 * @return the list of active movies
	 */
	public List<Movie> readActiveMovies() {
		try {
			openDb();
			List<Movie> moviesFound = new ArrayList<Movie>();
			for (int i = 0; i < movies.size(); i++) {
				if (movies.get(i).getStatus() != "Coming Soon") {
					moviesFound.add(movies.get(i));
				}
			}
			return moviesFound;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Read movie ranks by ratings
	 * 
	 * @param top
	 *            how many top items are required
	 * @return the list of top movies ranked by ratings
	 */
	public List<Movie> rankMoviesByRatings(int top) {
		try {
			openDb();
			ArrayList<Movie> rankedMovies = new ArrayList<Movie>(movies.size());
			for (int i = 0; i < movies.size(); i++) {
				rankedMovies.add(movies.get(i));
			}
			Collections.sort(rankedMovies, new Movie.CustomerRatingsComparator());

			ArrayList<Movie> topMovies = new ArrayList<Movie>(top);
			int limit = top > movies.size() ? movies.size() : top;
			for (int i = 0; i < limit; i++) {
				if (rankedMovies.get(i) != null)
					topMovies.add(rankedMovies.get(i));
			}
			return topMovies;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Read movie ranks by number of tickets sold
	 * 
	 * @param top
	 *            how many top items are required
	 * @return the list of top movies ranked by number of movie tickets sold
	 */
	public List<Movie> rankMoviesByTicketsSold(int top) {
		try {
			openDb();
			ArrayList<Movie> rankedMovies = new ArrayList<Movie>(movies.size());
			for (int i = 0; i < movies.size(); i++) {
				rankedMovies.add(movies.get(i));
			}
			Collections.sort(rankedMovies, new Movie.TicketSalesComparator());

			ArrayList<Movie> topMovies = new ArrayList<Movie>(top);
			int limit = top > movies.size() ? movies.size() : top;
			for (int i = 0; i < limit; i++) {
				if (rankedMovies.get(i) != null)
					topMovies.add(rankedMovies.get(i));
			}
			return topMovies;
		} catch (Exception e) {
			return null;
		}
	}
}
