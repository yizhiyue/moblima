package Dao;

import java.util.List;

import Entity.Cineplex;
import Entity.Movie;
import Entity.Schedule;

public interface ScheduleDAO {
	/**
	 * Read all schedules from data file.
	 */
	void openDb();

	/**
	 * Save all schedules from data file.
	 */
	void saveDb();

	/**
	 * Returns a list of all schedules.
	 * 
	 * @return a list of all schedules.
	 */
	List<Schedule> readAll();

	/**
	 * Returns the target schedule.
	 * @param id the id of the schedule object to be read
	 * @return the target schedule.
	 */
	Schedule read(long id);

	/**
	 * Update a schedule record.
	 * 
	 * @param id
	 *            the id of the schedule object to be updated
	 * @param schedule
	 *            the schedule object to be updated
	 * @return flag of success or failure of action
	 */
	boolean update(long id, Schedule schedule);

	/**
	 * Create a schedule record.
	 * 
	 * @param schedule
	 *            the schedule object to be created
	 * @return flag of success or failure of action
	 */
	boolean create(Schedule schedule);

	/**
	 * Delete a schedule record.
	 * 
	 * @param id
	 *            the id of the schedule object to be deleted
	 * @return flag of success or failure of action
	 */
	boolean delete(long id);

	/**
	 * Delete object.
	 * 
	 * @return flag of success or failure of action
	 */
	boolean deleteAll();

	/**
	 * Returns the target schedule.
	 * 
	 * @param cineplex
	 *            the target cineplex
	 * @param movie
	 *            the target movie
	 * @return the target movie schedules in the target cineplex
	 */
	List<Schedule> readByCineplexAndMovie(Cineplex cineplex, Movie movie);
}
