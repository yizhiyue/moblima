package Dao;

import java.util.List;

import Database.SerializeDB;
import Entity.Admin;
import Entity.Admin;

public class AdminDAOImpl implements AdminDAO {
	private static final String DBPATH = "data/dbAdmin.dat";
	private List<Admin> admins;

	public AdminDAOImpl() {
		
	}

	/**
	 * Read all admins from data file.
	 */
	public void openDb() {
		admins = SerializeDB.readSerializedObject(DBPATH);
	}

	/**
	 * Save all admins from data file.
	 */
	public void saveDb() {
		SerializeDB.writeSerializedObject(DBPATH, admins);
	}

	/**
	 * Returns a list of all admins.
	 * 
	 * @return a list of all admins.
	 */
	@Override
	public List<Admin> readAll() {
		openDb();
		return admins;
	}

	/**
	 * Returns the target admin.
	 * 
	 * @return the target admin.
	 */
	@Override
	public Admin read(int id) {
		openDb();
		Admin admin = new Admin();
		for (int i = 0; i < admins.size(); i++) {
			if (admins.get(i).getId() == id) {
				admin = admins.get(i);
				break;
			}
		}
		return admin;
	}

	public Admin login(String username, String password) {
		openDb();
		Admin admin = new Admin();
		for (int i = 0; i < admins.size(); i++) {
			if (admins.get(i).getUsername().equals(username) && admins.get(i).getPassword().equals(password)) {
				admin = admins.get(i);
				break;
			}
		}
		return admin;
	}
	
	/**
	 * Update a admin record.
	 */
	@Override
	public void update(int id, Admin admin) {
		openDb();
		for (int i = 0; i < admins.size(); i++) {
			if (admins.get(i).getId() == id) {
				admins.set(i, admin);
				break;
			}
		}
		saveDb();
	}

	/**
	 * Create a admin record.
	 */
	@Override
	public void create(Admin admin) {
		openDb();
		admins.add(admin);
		saveDb();
	}

	/**
	 * Delete a admin record.
	 */
	@Override
	public void delete(int id) {
		openDb();
		for (int i = 0; i < admins.size(); i++) {
			if (admins.get(i).getId() == id) {
				admins.remove(i);
				break;
			}
		}
		saveDb();
	}

	/**
	 * Delete object.
	 */
	@Override
	public void deleteAll() {
		openDb();
		admins.removeAll(null);
		saveDb();
	}

}
