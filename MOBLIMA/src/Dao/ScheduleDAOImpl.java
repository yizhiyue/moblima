package Dao;

import java.util.*;
import Database.SerializeDB;
import Entity.*;

public class ScheduleDAOImpl implements ScheduleDAO {
	private static final String DBPATH = "data/dbSchedule.dat";
	private List<Schedule> schedules;

	public ScheduleDAOImpl() {
		
	}

	/**
	 * Read all schedules from data file.
	 */
	public void openDb() {
		List<Schedule> output = SerializeDB.readSerializedObject(DBPATH);
		schedules =  output != null ? output : new ArrayList<Schedule>();
	}

	/**
	 * Save all schedules from data file.
	 */
	public void saveDb() {
		SerializeDB.writeSerializedObject(DBPATH, schedules);
	}

	/**
	 * Returns a list of all schedules.
	 * 
	 * @return a list of all schedules.
	 */
	@Override
	public List<Schedule> readAll() {
		try {
			openDb();
			return schedules;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Returns the target schedule.
	 * 
	 * @return the target schedule.
	 */
	@Override
	public Schedule read(long id) {
		try {
			openDb();
			Schedule schedule = null;
			for (int i = 0; i < schedules.size(); i++) {
				if (schedules.get(i).getId() == id) {
					schedule = schedules.get(i);
					break;
				}
			}
			return schedule;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Update a schedule record.
	 */
	@Override
	public boolean update(long id, Schedule schedule) {
		try {
			openDb();
			int i;
			for (i = 0; i < schedules.size(); i++) {
				if (schedules.get(i).getId() == id) {
					schedules.set(i, schedule);
					break;
				}
			}
			if(i == schedules.size())
				throw new Exception("Schedule not found.");
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Create a schedule record.
	 */
	@Override
	public boolean create(Schedule schedule) {
		try {
			openDb();
			schedules.add(schedule);
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Delete a schedule record.
	 */
	@Override
	public boolean delete(long id) {
		try {
			openDb();
			int i;
			for (i = 0; i < schedules.size(); i++) {
				if (schedules.get(i).getId() == id) 
					break;
			}
			if(i == schedules.size())
				throw new Exception("Schedule not found.");
			schedules.remove(i);
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Delete all schedule records.
	 */
	@Override
	public boolean deleteAll() {
		try {
			openDb();
			schedules.clear();
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	/**
	 * Returns the target schedule.
	 * 
	 * @return the target schedule.
	 */
	@Override
	public List<Schedule> readByCineplexAndMovie(Cineplex cineplex, Movie movie) {
		try {
			openDb();
			List<Schedule> schedulesFound = new ArrayList<Schedule>();
			for (int i = 0; i < schedules.size(); i++) {
				if (schedules.get(i).getCinema().getCineplex().getId() == cineplex.getId() && schedules.get(i).getMovie().getId() == movie.getId()) {
					schedulesFound.add(schedules.get(i));
				}
			}
			if(schedulesFound.size() == 0)
				throw new Exception("Schedule not found.");
			return schedulesFound;
		} catch (Exception e) {
			return null;
		}
	}
}
