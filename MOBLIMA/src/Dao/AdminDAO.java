package Dao;

import java.util.List;

import Entity.Admin;

public interface AdminDAO {
	/**
	 * Read all admins from data file.
	 */
	void openDb();

	/**
	 * Save all admins from data file.
	 */
	void saveDb();

	/**
	 * Returns a list of all admins.
	 * 
	 * @return a list of all admins.
	 */
	List<Admin> readAll();

	/**
	 * Returns the target admin.
	 * 
	 * @param id
	 *            the id of the object to be read
	 * @return the target admin.
	 */
	Admin read(int id);

	/**
	 * Update a admin record.
	 * 
	 * @param id
	 *            the id of admin to be updated
	 * @param admin
	 *            the admin object to be updated
	 */
	void update(int id, Admin admin);

	/**
	 * Create a admin record.
	 * 
	 * @param admin
	 *            the admin object to be created
	 */
	void create(Admin admin);

	/**
	 * Delete a admin record.
	 * 
	 * @param id
	 *            the id of admin object to be deleted
	 */
	void delete(int id);

	/**
	 * Delete object.
	 */
	void deleteAll();
}
