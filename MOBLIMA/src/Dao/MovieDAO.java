package Dao;

import java.util.List;

import Entity.Movie;

public interface MovieDAO {
	/**
	 * Read all movies from data file.
	 */
	void openDb();

	/**
	 * Save all movies from data file.
	 */
	void saveDb();

	/**
	 * Returns a list of all movies.
	 * 
	 * @return a list of all movies.
	 */
	List<Movie> readAll();

	/**
	 * Returns the target movie.
	 * @param id the id of the movie object to be read
	 * @return the target movie.
	 */
	Movie read(long id);

	/**
	 * Update a movie record.
	 * 
	 * @param id
	 *            the id of movie object to be updated
	 * @param movie
	 *            the movie object to be updated
	 * @return flag of success or failure of action
	 */
	boolean update(long id, Movie movie);

	/**
	 * Create a movie record.
	 * 
	 * @param movie
	 *            the movie object to be deleted
	 * @return flag of success or failure of action
	 */
	boolean create(Movie movie);

	/**
	 * Delete a movie record.
	 * 
	 * @param id
	 *            the id of movie object to be deleted
	 * @return flag of success or failure of action
	 */
	boolean delete(long id);

	/**
	 * Delete object.
	 * 
	 * @return flag of success or failure of action
	 */
	boolean deleteAll();
}
