package Dao;

import java.util.List;

import Entity.Holiday;

public interface HolidayDAO {
	/**
	 * Read all holidays from data file.
	 */
	void openDb();

	/**
	 * Save all holidays from data file.
	 */
	void saveDb();

	/**
	 * Returns a list of all holidays.
	 * 
	 * @return a list of all holidays.
	 */
	List<Holiday> readAll();

	/**
	 * Returns the target holiday.
	 * 
	 * @param id
	 *            the id of holiday object to be read
	 * @return the target holiday.
	 */
	Holiday read(long id);

	/**
	 * Update a holiday record.
	 * 
	 * @param id
	 *            the id of holiday object to be updated
	 * @param holiday
	 *            the holiday object to be updated
	 * @return flag of success or failure of action
	 */
	boolean update(long id, Holiday holiday);

	/**
	 * Create a holiday record.
	 * 
	 * @param holiday
	 *            the holiday object to be created
	 * @return flag of success or failure of action
	 */
	boolean create(Holiday holiday);

	/**
	 * Delete a holiday record.
	 * 
	 * @param id
	 *            the id of holiday object to be deleted
	 * @return flag of success or failure of action
	 */
	boolean delete(long id);

	/**
	 * Delete object.
	 * 
	 * @return flag of success or failure of action
	 */
	boolean deleteAll();
}
