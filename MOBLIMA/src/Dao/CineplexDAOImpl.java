package Dao;

import java.util.*;
import Database.SerializeDB;
import Entity.Cineplex;

public class CineplexDAOImpl implements CineplexDAO {
	private static final String DBPATH = "data/dbCineplex.dat";
	private List<Cineplex> cineplexs;

	public CineplexDAOImpl() {
		
	}

	/**
	 * Read all cineplexs from data file.
	 */
	public void openDb() {
		List<Cineplex> output = SerializeDB.readSerializedObject(DBPATH);
		cineplexs =  output != null ? output : new ArrayList<Cineplex>();
	}

	/**
	 * Save all cineplexs from data file.
	 */
	public void saveDb() {
		SerializeDB.writeSerializedObject(DBPATH, cineplexs);
	}

	/**
	 * Returns a list of all cineplexs.
	 * 
	 * @return a list of all cineplexs.
	 */
	@Override
	public List<Cineplex> readAll() {
		try {
			openDb();
			return cineplexs;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Returns the target cineplex.
	 * 
	 * @return the target cineplex.
	 */
	@Override
	public Cineplex read(long id) {
		try {
			openDb();
			Cineplex cineplex = null;
			for (int i = 0; i < cineplexs.size(); i++) {
				if (cineplexs.get(i).getId() == id) {
					cineplex = cineplexs.get(i);
					break;
				}
			}
			return cineplex;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Update a cineplex record.
	 */
	@Override
	public boolean update(long id, Cineplex cineplex) {
		try {
			openDb();
			int i;
			for (i = 0; i < cineplexs.size(); i++) {
				if (cineplexs.get(i).getId() == id) {
					cineplexs.set(i, cineplex);
					break;
				}
			}
			if(i == cineplexs.size())
				throw new Exception("Cineplex not found.");
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Create a cineplex record.
	 */
	@Override
	public boolean create(Cineplex cineplex) {
		try {
			openDb();
			cineplexs.add(cineplex);
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Delete a cineplex record.
	 */
	@Override
	public boolean delete(long id) {
		try {
			openDb();
			int i;
			for (i = 0; i < cineplexs.size(); i++) {
				if (cineplexs.get(i).getId() == id)
					break;
			}
			if(i == cineplexs.size())
				throw new Exception("Cineplex not found.");
			cineplexs.remove(i);
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Delete all cineplex records.
	 */
	@Override
	public boolean deleteAll() {
		try {
			openDb();
			cineplexs.clear();
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
