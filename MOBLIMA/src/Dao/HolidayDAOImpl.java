package Dao;

import java.util.*;
import Database.SerializeDB;
import Entity.Holiday;

public class HolidayDAOImpl implements HolidayDAO {
	private static final String DBPATH = "data/dbHoliday.dat";
	private List<Holiday> holidays;

	public HolidayDAOImpl() {
		
	}

	/**
	 * Read all holidays from data file.
	 */
	public void openDb() {
		List<Holiday> output = SerializeDB.readSerializedObject(DBPATH);
		holidays =  output != null ? output : new ArrayList<Holiday>();
	}

	/**
	 * Save all holidays from data file.
	 */
	public void saveDb() {
		SerializeDB.writeSerializedObject(DBPATH, holidays);
	}

	/**
	 * Returns a list of all holidays.
	 * 
	 * @return a list of all holidays.
	 */
	@Override
	public List<Holiday> readAll() {
		try {
			openDb();
			return holidays;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Returns the target holiday.
	 * 
	 * @return the target holiday.
	 */
	@Override
	public Holiday read(long id) {
		try {
			openDb();
			Holiday holiday = null;
			for (int i = 0; i < holidays.size(); i++) {
				if (holidays.get(i).getId() == id) {
					holiday = holidays.get(i);
					break;
				}
			}
			return holiday;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Update a holiday record.
	 */
	@Override
	public boolean update(long id, Holiday holiday) {
		try {
			openDb();
			int i;
			for (i = 0; i < holidays.size(); i++) {
				if (holidays.get(i).getId() == id) {
					holidays.set(i, holiday);
					break;
				}
			}
			if(i == holidays.size())
				throw new Exception("Holiday not found.");
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Create a holiday record.
	 */
	@Override
	public boolean create(Holiday holiday) {
		try {
			openDb();
			holidays.add(holiday);
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Delete a holiday record.
	 */
	@Override
	public boolean delete(long id) {
		try {
			openDb();
			int i;
			for (i = 0; i < holidays.size(); i++) {
				if (holidays.get(i).getId() == id)	
					break;
			}
			if(i == holidays.size())
				throw new Exception("Holiday not found.");
			holidays.remove(i);
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Delete all holiday records.
	 */
	@Override
	public boolean deleteAll() {
		try {
			openDb();
			holidays.clear();
			saveDb();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
}
