package Dao;

import java.util.List;

import Entity.Cineplex;

public interface CineplexDAO {
	/**
	 * Read all cineplexs from data file.
	 */
	void openDb();

	/**
	 * Save all cineplexs from data file.
	 */
	void saveDb();

	/**
	 * Returns a list of all cineplexs.
	 * 
	 * @return a list of all cineplexs.
	 */
	List<Cineplex> readAll();

	/**
	 * Returns the target cineplex.
	 * 
	 * @param id
	 *            the id of cineplex object to be read
	 * @return the target cineplex.
	 */
	Cineplex read(long id);

	/**
	 * Update a cineplex record.
	 * 
	 * @param id
	 *            the id of cineplex object to be updated
	 * @param cineplex
	 *            the cineplex object to be updated
	 * @return flag of success or failure of action
	 */
	boolean update(long id, Cineplex cineplex);

	/**
	 * Create a cineplex record.
	 * 
	 * @param cineplex
	 *            the cineplex object to be created
	 * @return flag of success or failure of action
	 */
	boolean create(Cineplex cineplex);

	/**
	 * Delete a cineplex record.
	 * 
	 * @param id
	 *            the id of cineplex object to be deleted
	 * @return flag of success or failure of action
	 */
	boolean delete(long id);

	/**
	 * Delete object.
	 * @return flag of success or failure of action
	 */
	boolean deleteAll();
}
