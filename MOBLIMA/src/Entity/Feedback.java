package Entity;

import java.io.Serializable;

public class Feedback implements Serializable {
	private int points;
	private String review;

	public Feedback() {
		super();
	}

	public Feedback(int points, String review) {
		super();
		this.points = points;
		this.review = review;
	}

	/**
	 * @return the points
	 */
	public int getPoints() {
		return points;
	}

	/**
	 * @param points
	 *            the points to set
	 */
	public void setPoints(int points) {
		this.points = points;
	}

	/**
	 * @return the review
	 */
	public String getReview() {
		return review;
	}

	/**
	 * @param review
	 *            the review to set
	 */
	public void setReview(String review) {
		this.review = review;
	}

}
