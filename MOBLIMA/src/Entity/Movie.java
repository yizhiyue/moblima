package Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import Dao.MovieDAOImpl;

public class Movie implements Serializable {
	private long id;
	private String title;
	private String status;
	private String synopsis;
	private String director;
	private String movieRating;
	private MovieType[] movieType;
	private int duration;
	private String[] casts;
	private Calendar endOfDate;
	private ArrayList<Feedback> feedback;
	private int noOfTicketSold;

	public enum MovieType {
		ACTION, COMEDY, HORROR, SCIFI, THREE_D, BLOCKBUSTER
	}

	public Movie() {
		super();
	}

	public Movie(String title, String status, String movieRating, String synopsis, String director,
			MovieType[] movieType, int duration, String[] casts, Calendar endOfDate, ArrayList<Feedback> feedback,
			int noOfTicketSold) {
		super();
		this.id = new Date().getTime();
		this.title = title;
		this.status = status;
		this.synopsis = synopsis;
		this.director = director;
		this.movieType = movieType;
		this.casts = casts;
		this.duration = duration;
		this.movieRating = movieRating;
		this.endOfDate = endOfDate;
		this.feedback = feedback;
		this.noOfTicketSold = noOfTicketSold;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the synopsis
	 */
	public String getSynopsis() {
		return synopsis;
	}

	/**
	 * @param synopsis
	 *            the synopsis to set
	 */
	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	/**
	 * @return the director
	 */
	public String getDirector() {
		return director;
	}

	/**
	 * @param director
	 *            the director to set
	 */
	public void setDirector(String director) {
		this.director = director;
	}

	/**
	 * @return the movieRating
	 */
	public String getMovieRating() {
		return movieRating;
	}

	/**
	 * @param movieRating
	 *            the movieRating to set
	 */
	public void setMovieRating(String movieRating) {
		this.movieRating = movieRating;
	}

	/**
	 * @return the movieType
	 */
	public MovieType[] getMovieType() {
		return movieType;
	}

	/**
	 * @param movieType
	 *            the movieType to set
	 */
	public void setMovieType(MovieType[] movieType) {
		this.movieType = movieType;
	}

	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @param duration
	 *            the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * @return the casts
	 */
	public String[] getCasts() {
		return casts;
	}

	/**
	 * @param casts
	 *            the casts to set
	 */
	public void setCasts(String[] casts) {
		this.casts = casts;
	}

	/**
	 * @return the endOfDate
	 */
	public Calendar getEndOfDate() {
		return endOfDate;
	}

	/**
	 * @param endOfDate
	 *            the endOfDate to set
	 */
	public void setEndOfDate(Calendar endOfDate) {
		this.endOfDate = endOfDate;
	}

	/**
	 * @return the feedback
	 */
	public ArrayList<Feedback> getFeedback() {
		return feedback;
	}

	/**
	 * @param feedback
	 *            the feedback to set
	 */
	public void setFeedback(ArrayList<Feedback> feedback) {
		this.feedback = feedback;
	}

	/**
	 * @return the noOfTicketSold
	 */
	public int getNoOfTicketSold() {
		return noOfTicketSold;
	}

	/**
	 * @param noOfTicketSold
	 *            the noOfTicketSold to set
	 */
	public void setNoOfTicketSold(int noOfTicketSold) {
		this.noOfTicketSold = noOfTicketSold;
	}

	public static double getAdditionalTicketPrice(Movie.MovieType movieType) {
		switch (movieType) {
		case THREE_D:
			return 4;
		case BLOCKBUSTER:
			return 2;
		default:
			return 0;
		}
	}

	/**
	 * Calculate total additional ticket price added on to the booking based on the
	 * list of movie types
	 * 
	 * @param movieTypes
	 *            array of movie types
	 * @return the value of additional price
	 */
	public static double getTotalAdditionalTicketPrice(Movie.MovieType[] movieTypes) {
		double additionalPrice = 0;
		for (int i = 0; i < movieTypes.length; i++) {
			additionalPrice += getAdditionalTicketPrice(movieTypes[i]);
		}
		return additionalPrice;
	}

	public static class CustomerRatingsComparator implements Comparator<Movie> {
		public int compare(Movie o1, Movie o2) {
			ArrayList<Feedback> feedbacks1 = o1.getFeedback();
			ArrayList<Feedback> feedbacks2 = o2.getFeedback();
			int total1 = 0;
			int total2 = 0;
			for (int i = 0; i < feedbacks1.size(); i++)
				total1 += feedbacks1.get(i).getPoints();

			for (int i = 0; i < feedbacks1.size(); i++)
				total2 += feedbacks2.get(i).getPoints();

			Double avgPoint1 = (double) 0;
			Double avgPoint2 = (double) 0;

			if (feedbacks1.size() > 0 && feedbacks2.size() > 0) {
				avgPoint1 = (double) total1 / feedbacks1.size();
				avgPoint2 = (double) total2 / feedbacks2.size();
			}
			return -avgPoint1.compareTo(avgPoint2);
		}
	}

	public static class TicketSalesComparator implements Comparator<Movie> {
		public int compare(Movie o1, Movie o2) {
			return -Integer.compare(o1.getNoOfTicketSold(), o2.getNoOfTicketSold());
		}
	}
}
