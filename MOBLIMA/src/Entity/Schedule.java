package Entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Schedule implements Serializable {
	private long id;
	private Movie movie;
	private Cinema cinema;
	private Calendar showingDateTime;
	private ArrayList<Booking> bookings;

	public Schedule() {
		super();
	}

	public Schedule(Movie movie, Cinema cinema, Calendar showingDateTime) {
		super();
		this.id = new Date().getTime();
		this.movie = movie;
		this.cinema = cinema;
		this.showingDateTime = showingDateTime;
		this.bookings = new ArrayList<Booking>();
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the movie
	 */
	public Movie getMovie() {
		return movie;
	}

	/**
	 * @param movie the movie to set
	 */
	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	/**
	 * @return the cinema
	 */
	public Cinema getCinema() {
		return cinema;
	}

	/**
	 * @param cinema the cinema to set
	 */
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}

	/**
	 * @return the showingDateTime
	 */
	public Calendar getShowingDateTime() {
		return showingDateTime;
	}

	/**
	 * @param showingDateTime the showingDateTime to set
	 */
	public void setShowingDateTime(Calendar showingDateTime) {
		this.showingDateTime = showingDateTime;
	}

	/**
	 * @return the bookings
	 */
	public ArrayList<Booking> getBookings() {
		return bookings;
	}

	/**
	 * @param bookings the bookings to set
	 */
	public void setBookings(ArrayList<Booking> bookings) {
		this.bookings = bookings;
	}

	

	
}
