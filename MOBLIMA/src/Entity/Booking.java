package Entity;

import java.io.Serializable;
import java.util.Date;

public class Booking implements Serializable {
	private long id;
	private Schedule schedule;
	private Seat[] seats;
	private Customer customer;
	private String transactionId;
	private Double transactionAmount;
	private Boolean isPaymentSuccess;

	public Booking() {
		super();
		this.id = new Date().getTime();
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the schedule
	 */
	public Schedule getSchedule() {
		return schedule;
	}

	/**
	 * @param schedule
	 *            the schedule to set
	 */
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	/**
	 * @return the seats
	 */
	public Seat[] getSeats() {
		return seats;
	}

	/**
	 * @param seats
	 *            the seats to set
	 */
	public void setSeats(Seat[] seats) {
		this.seats = seats;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer
	 *            the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the transactionAmount
	 */
	public Double getTransactionAmount() {
		return transactionAmount;
	}

	/**
	 * @param transactionAmount
	 *            the transactionAmount to set
	 */
	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	/**
	 * @return the isPaymentSuccess
	 */
	public Boolean getIsPaymentSuccess() {
		return isPaymentSuccess;
	}

	/**
	 * @param isPaymentSuccess
	 *            the isPaymentSuccess to set
	 */
	public void setIsPaymentSuccess(Boolean isPaymentSuccess) {
		this.isPaymentSuccess = isPaymentSuccess;
	}

}
