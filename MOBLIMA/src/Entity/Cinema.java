package Entity;

import java.io.Serializable;
import java.util.Date;

public class Cinema implements Serializable {
	private String name;
	private String shortCode;
	private CinemaClass cinemaClass;
	private Cineplex cineplex;
	private String[][] seatsAvailable;
	private int totalSeat;

	public enum CinemaClass {
		PLATINUM, GOLD, SILVER, NORMAL
	}

	public Cinema() {
		super();
	}

	public Cinema(String name, String shortCode, CinemaClass cinemaClass, Cineplex cineplex, String[][] seatsAvailable,
			int totalSeat) {
		super();
		this.name = name;
		this.shortCode = shortCode;
		this.cinemaClass = cinemaClass;
		this.cineplex = cineplex;
		this.seatsAvailable = seatsAvailable;
		this.totalSeat = totalSeat;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the shortCode
	 */
	public String getShortCode() {
		return shortCode;
	}

	/**
	 * @param shortCode
	 *            the shortCode to set
	 */
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	/**
	 * @return the cinemaClass
	 */
	public CinemaClass getCinemaClass() {
		return cinemaClass;
	}

	/**
	 * @param cinemaClass
	 *            the cinemaClass to set
	 */
	public void setCinemaClass(CinemaClass cinemaClass) {
		this.cinemaClass = cinemaClass;
	}

	/**
	 * @return the cineplex
	 */
	public Cineplex getCineplex() {
		return cineplex;
	}

	/**
	 * @param cineplex
	 *            the cineplex to set
	 */
	public void setCineplex(Cineplex cineplex) {
		this.cineplex = cineplex;
	}

	/**
	 * @return the seatsAvailable
	 */
	public String[][] getSeatsAvailable() {
		return seatsAvailable;
	}

	/**
	 * @param seatsAvailable
	 *            the seatsAvailable to set
	 */
	public void setSeatsAvailable(String[][] seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	/**
	 * @return the totalSeat
	 */
	public int getTotalSeat() {
		return totalSeat;
	}

	/**
	 * @param totalSeat
	 *            the totalSeat to set
	 */
	public void setTotalSeat(int totalSeat) {
		this.totalSeat = totalSeat;
	}

	/**
	 * Calculate ticket price based on cinema class
	 * @return the value of ticket price
	 */

	public double getTicketPrice() {
		switch (this.cinemaClass) {
		case PLATINUM:
			return 16;
		case GOLD:
			return 14;
		case SILVER:
			return 12;
		default:
			return 10;
		}
	}

}
