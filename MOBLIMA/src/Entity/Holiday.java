package Entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Holiday implements Serializable {
	private long id;
	private String name;
	private Calendar date;

	public Holiday() {
		super();
	}

	public Holiday(String name, Calendar date) {
		super();
		this.id = new Date().getTime();
		this.name = name;
		this.date = date;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the date
	 */
	public Calendar getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Calendar date) {
		this.date = date;
	}

}
