package Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Cineplex implements Serializable {
	private long id;
	private String name;
	private String shortCode;
	private String address;
	private String contact;
	private ArrayList<Cinema> cinemas;

	public Cineplex() {
		super();
	};

	public Cineplex(String name, String shortCode, String address, String contact, ArrayList<Cinema> cinemas) {
		super();
		this.id = new Date().getTime();
		this.name = name;
		this.shortCode = shortCode;
		this.address = address;
		this.contact = contact;
		this.cinemas = cinemas;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the shortCode
	 */
	public String getShortCode() {
		return shortCode;
	}

	/**
	 * @param shortCode
	 *            the shortCode to set
	 */
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the contact
	 */
	public String getContact() {
		return contact;
	}

	/**
	 * @param contact
	 *            the contact to set
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * @return the cinemas
	 */
	public ArrayList<Cinema> getCinemas() {
		return cinemas;
	}

	/**
	 * @param cinemas
	 *            the cinemas to set
	 */
	public void setCinemas(ArrayList<Cinema> cinemas) {
		this.cinemas = cinemas;
	}

}
