package Control;

import java.util.*;
import Dao.*;
import Entity.*;
import Entity.Movie.MovieType;
import Helper.InputHelper;
import Helper.OutputHelper;

public class MovieController {
	private MovieDAOImpl movieDaoImpl;
	private final static String STR_MOVIE_NOT_FOUND = "Sorry, no movie is found.";
	private final static String STR_MOVIE_TYPE_NOT_FOUND = "Sorry, no movie is found.";

	public MovieController() {
		movieDaoImpl = new MovieDAOImpl();
	}

	/**
	 * List all the movies stored in flat DB
	 */
	public void listAllMovies() {
		List<Movie> movies = movieDaoImpl.readAll();
		System.out.println();
		if (movies != null && movies.size() > 0) {
			OutputHelper.printMovieList(movies, false);
		} else {
			System.out.println(STR_MOVIE_NOT_FOUND);
		}
	}

	/**
	 * List ranked the movies by ratings stored in flat DB
	 */
	public void listRankedMoviesByRating() {
		List<Movie> movies = movieDaoImpl.rankMoviesByRatings(5);
		System.out.println();
		if (movies != null && movies.size() > 0) {
			OutputHelper.printRankedByRatingMovieList(movies);
		} else {
			System.out.println(STR_MOVIE_NOT_FOUND);
		}
	}

	/**
	 * List ranked the movies by ticket sold stored in flat DB
	 */
	public void listRankedMoviesByTicketSold() {
		List<Movie> movies = movieDaoImpl.rankMoviesByTicketsSold(5);
		System.out.println();
		if (movies != null && movies.size() > 0) {
			OutputHelper.printRankedByTicketsSoldMovieList(movies);
		} else {
			System.out.println(STR_MOVIE_NOT_FOUND);
		}
	}

	/**
	 * Search specific movie
	 */
	public void searchMovie() {
		System.out.println("Input movie name key word to search: ");
		String keyword = InputHelper.getString();
		List<Movie> moviesFound = movieDaoImpl.readByTitle(keyword);
		if (moviesFound != null && moviesFound.size() > 0) {
			OutputHelper.printMovieList(moviesFound, false);
		} else {
			System.out.println(STR_MOVIE_NOT_FOUND);
		}
	}

	/**
	 * Display a movie detail
	 */
	public void viewMovie() {
		System.out.println("Input movie ID to view details: ");
		long movieId = InputHelper.getLong();
		Movie movieFound = movieDaoImpl.read(movieId);
		if (movieFound != null) {
			OutputHelper.printMovieItem(movieFound, 0, true, false);
		} else {
			System.out.println(STR_MOVIE_NOT_FOUND);
		}
	}

	/**
	 * add a new movie to database
	 */
	public void addMovie() {
		int choice;
		boolean result;
		String title = "";
		String status = "";
		String synopsis = "";
		String director = "";
		Movie.MovieType[] movieTypes = null;
		String movieRating = "";
		int noOfCasts;
		int duration;
		String[] casts;
		Calendar endOfDate;

		System.out.println("Title: ");
		title = InputHelper.getString();

		System.out.println("Synopsis: ");
		synopsis = InputHelper.getString();

		System.out.println("Director: ");
		director = InputHelper.getString();

		System.out.println("Rating: ");
		movieRating = inputRating();

		MovieType[] movieTypeList = Movie.MovieType.values();
		System.out.println("Moive Type: ");
		movieTypes = inputMovieType(movieTypeList);

		System.out.println("Duration (in minutes): ");
		duration = InputHelper.getInt();

		System.out.println("Status: (Select an option)");
		status = inputStatus();

		System.out.println("No of Casts: ");
		casts = inputCasts();

		System.out.println("End of Date: ");
		endOfDate = InputHelper.getDateTime();

		Movie newMovie = new Movie(title, status, movieRating, synopsis, director, movieTypes, duration, casts,
				endOfDate, new ArrayList<Feedback>(), 0);
		result = movieDaoImpl.create(newMovie);
		if (result) {
			System.out.println("Movie added successfully.");
		} else {
			System.out.println("Failed adding the movie.");
		}
	}

	/**
	 * remove a movie from database
	 */
	public void removeMovie() {
		System.out.println("Input movie ID to remove: ");
		long id = InputHelper.getLong();
		boolean result = movieDaoImpl.delete(id);
		if (result)
			System.out.println("Deleted " + id + " Successfully");
		else
			System.out.println("Failed to delete movie " + id);
	}

	/**
	 * update a movie in database
	 */
	public void updateMovie() {
		System.out.println("Input movie ID to update: ");
		long id = InputHelper.getLong();
		Movie movie = movieDaoImpl.read(id);
		if (movie != null && movie.getId() != 0) {
			OutputHelper.printMovieItem(movie, 0, false, false);

			int choice = 0;
			do {
				System.out.println("Select a property to update.");
				System.out.println("1. Title");
				System.out.println("2. Type");
				System.out.println("3. Status");
				System.out.println("4. Director");
				System.out.println("5. Casts");
				System.out.println("6. Synopsis");
				System.out.println("7. Rating");
				System.out.println("8. Duration");
				System.out.println("0. Back");

				choice = InputHelper.getInt();
				switch (choice) {
				case 1:
					System.out.println("New Title: ");
					movie.setTitle(InputHelper.getString());
					break;
				case 2:
					MovieType[] movieTypeList = Movie.MovieType.values();
					System.out.println("New Type: ");
					movie.setMovieType(inputMovieType(movieTypeList));
					break;
				case 3:
					System.out.println("New Status: ");
					movie.setStatus(InputHelper.getString());
					break;
				case 4:
					System.out.println("New Director: ");
					movie.setDirector(InputHelper.getString());
					break;
				case 5:
					System.out.println("New Casts: ");
					movie.setCasts(inputCasts());
					break;
				case 6:
					System.out.println("New Synopsis: ");
					movie.setSynopsis(InputHelper.getString());
					break;
				case 7:
					System.out.println("Rating: ");
					movie.setMovieRating(inputRating());
					break;
				case 8:
					System.out.println("Duration: ");
					movie.setDuration(InputHelper.getInt());
					break;
				}
				movieDaoImpl.update(movie.getId(), movie);
				OutputHelper.printMovieItem(movie, 0, false, false);
			} while (choice != 0);
		} else
			System.out.println(STR_MOVIE_NOT_FOUND);
	}

	/**
	 * feedback by rating and reviewing a movie and update to database
	 */
	public void feedbackMovie() {
		List<Movie> movies = movieDaoImpl.readActiveMovies();
		Movie movieSelected = InputMovie(movies);

		Feedback feedback = new Feedback();

		System.out.println("Input your review: ");
		feedback.setReview(InputHelper.getString());

		System.out.println("Input your rating point from 0 to 5: ");
		boolean isValid = false;
		while (!isValid) {
			int points = InputHelper.getInt();
			if (points >= 0 && points <= 5) {
				feedback.setPoints(points);
				isValid = true;
			} else
				System.out.println("Your input should range from 0 to 5. Please try again.");
		}

		movieSelected.getFeedback().add(feedback);
		boolean result = movieDaoImpl.update(movieSelected.getId(), movieSelected);

		if (result)
			System.out.println("Your feedback has been added.");
		else
			System.out.println("Sorry, you feedback is not added. Some errors just happened.");
	}

	/**
	 * input status of a movie item
	 * 
	 * @return the status which user input
	 * 
	 */
	private static String inputStatus() {
		String status = "";
		do {
			System.out.println("1. Coming Soon");
			System.out.println("2. Preview");
			System.out.println("3. Now Showing");
			int choice = InputHelper.getInt();
			switch (choice) {
			case 1:
				status = "Coming Soon";
				break;
			case 2:
				status = "Preview";
				break;
			case 3:
				status = "Now Showing";
				break;
			}
		} while (status.isEmpty());

		return status;
	}

	/**
	 * input rating of a movie item
	 * 
	 * @return the rating which user input
	 */
	private static String inputRating() {
		String movieRating = "";
		do {
			System.out.println("1. G");
			System.out.println("2. PG");
			System.out.println("3. PG13");
			System.out.println("4. NC16");
			System.out.println("5. M18");
			System.out.println("6. R21");
			int choice = InputHelper.getInt();
			switch (choice) {
			case 1:
				movieRating = "G";
				break;
			case 2:
				movieRating = "PG";
				break;
			case 3:
				movieRating = "PC13";
				break;
			case 4:
				movieRating = "NC16";
				break;
			case 5:
				movieRating = "M18";
				break;
			case 6:
				movieRating = "R21";
				break;
			}
		} while (movieRating.isEmpty());

		return movieRating;
	}

	/**
	 * input a list of casts for a movie item
	 * 
	 * @return the cast list which user input
	 */
	private static String[] inputCasts() {
		int noOfCasts;
		String[] casts;
		do {
			noOfCasts = InputHelper.getInt();
			if (noOfCasts < 2)
				System.out.println("Sorry, your input is not valid. The number of casts is at least 2. ");
		} while (noOfCasts < 2);
		casts = new String[noOfCasts];
		for (int i = 0; i < noOfCasts; i++) {
			System.out.print((i + 1) + ": ");
			casts[i] = InputHelper.getString();
		}
		return casts;
	}

	/**
	 * input selection of movie
	 * 
	 * @param movieTypeList
	 *            list of movieTypes
	 * @return the movie type selected
	 */
	private static Movie.MovieType[] inputMovieType(MovieType[] movieTypeList) {
		int choice = 0;
		MovieType[] movieTypesSelected = null;

		OutputHelper.printMovieTypeList(movieTypeList);

		// Get no of movie types are needed from users
		int movieTypeLength = 0;
		do {
			System.out.println("How many movie types are to be selected: ");
			movieTypeLength = InputHelper.getInt();
		} while (movieTypeLength <= 0);

		movieTypesSelected = new MovieType[movieTypeLength];
		// User input required number of movie types
		for (int i = 0; i < movieTypeLength; i++) {
			System.out.println("Select a movie type by input the index: ");
			choice = InputHelper.getInt();
			if (choice >= 1 && choice <= movieTypeList.length) {
				movieTypesSelected[i] = movieTypeList[choice - 1];
			} else {
				System.out.println(STR_MOVIE_TYPE_NOT_FOUND);
			}
		}
		System.out.println("You have selected: ");
		OutputHelper.printMovieTypeList(movieTypesSelected);

		return movieTypesSelected;
	}

	/**
	 * input selection of movie
	 * 
	 * @param movies
	 *            list of movies
	 * @return the movie selected
	 */
	private static Movie InputMovie(List<Movie> movies) {
		int choice = 0;
		Movie movieSelected = null;
		boolean isSelected = false;

		OutputHelper.printMovieList(movies, true);

		do {
			System.out.println("Select a movie by input the index: ");
			choice = InputHelper.getInt();
			if (choice >= 1 && choice <= movies.size()) {
				movieSelected = movies.get(choice - 1);
				isSelected = true;
			} else {
				System.out.println(STR_MOVIE_NOT_FOUND);
			}

		} while (movieSelected == null);

		return movieSelected;
	}
}
