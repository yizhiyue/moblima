package Control;

import java.util.*;
import Dao.*;
import Entity.*;
import Entity.Holiday;
import Helper.InputHelper;
import Helper.OutputHelper;

public class HolidayController {
	private HolidayDAOImpl holidayDaoImpl;
	private final static String STR_HOLIDAY_NOT_FOUND = "Sorry,holiday is found.";
	private final static String STR_HOLIDAY_TYPE_NOT_FOUND = "Sorry, no holiday is found.";

	public HolidayController() {
		holidayDaoImpl = new HolidayDAOImpl();
	}

	/**
	 * List all the holidays stored in flat DB
	 */
	public void listAllHolidays() {
		List<Holiday> holidays = holidayDaoImpl.readAll();
		System.out.println();
		if (holidays != null && holidays.size() > 0) {
			OutputHelper.printHolidayList(holidays);
		} else {
			System.out.println(STR_HOLIDAY_NOT_FOUND);
		}
	}

	/**
	 * add a new holiday to database
	 */
	public void addHoliday() {
		int choice;
		boolean result;
		String name = "";
		Calendar date;

		System.out.println("Holiday Name: ");
		name = InputHelper.getString();

		System.out.println("Holiday Date: ");
		date = InputHelper.getDate();

		Holiday newHoliday = new Holiday(name,date);
		result = holidayDaoImpl.create(newHoliday);
		if (result) {
			System.out.println("Holiday added successfully.");
		} else {
			System.out.println("Failed adding the holiday.");
		}
	}

	/**
	 * remove a holiday from database
	 */
	public void removeHoliday() {
		System.out.println("Input holiday ID to remove: ");
		long id = InputHelper.getLong();
		boolean result = holidayDaoImpl.delete(id);
		if (result)
			System.out.println("Deleted " + id + " Successfully");
		else
			System.out.println("Failed to delete holiday " + id);
	}

	/**
	 * update a holiday in database
	 */
	public void updateHoliday() {
		System.out.println("Input holiday ID to update: ");
		long id = InputHelper.getLong();
		Holiday holiday = holidayDaoImpl.read(id);
		if (holiday != null && holiday.getId() != 0) {

			int choice = 0;
			do {
				System.out.println("Select a property to update.");
				System.out.println("1. Name");
				System.out.println("2. Date");
				System.out.println("0. Back");
			
				choice = InputHelper.getInt();
				switch (choice) {
				case 1:
					System.out.println("New Name: ");
					holiday.setName(InputHelper.getString());
					break;
				
				case 2:
					System.out.println("New Date: ");
					holiday.setDate(InputHelper.getDate());
					break;	
				}
				holidayDaoImpl.update(holiday.getId(), holiday);
				System.out.println("Holiday is updated.");
			} while (choice != 0);
		} else
			System.out.println(STR_HOLIDAY_NOT_FOUND);
	}

	
}

