package Control;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import Dao.*;
import Entity.Booking;
import Entity.Seat;
import Entity.Cinema;
import Entity.Cineplex;
import Entity.Customer;
import Entity.Holiday;
import Entity.Movie;
import Entity.Schedule;
import Helper.InputHelper;
import Helper.OutputHelper;

public class BookingController {
	private ScheduleDAOImpl scheduleDaoImpl;
	private CineplexDAOImpl cineplexDaoImpl;
	// private CinemaDAOImpl cinemaDaoImpl;
	private MovieDAOImpl movieDaoImpl;
	private HolidayDAOImpl holidayDaoImpl;
	private final static String STR_BOOKING_NOT_FOUND = "Sorry, no booking record is found.";
	private final static String STR_SCHEDULE_NOT_FOUND = "Sorry, no schedule is found.";
	private final static String STR_CINEPLEX_NOT_FOUND = "Sorry, no cineplex is found.";
	private final static String STR_CINEMA_NOT_FOUND = "Sorry, no cinema is found.";
	private final static String STR_MOVIE_NOT_FOUND = "Sorry, no movie is found.";
	private final static String STR_PRICE_BREAKDOWN_TABLE_FORMAT_WITH_INDEX = "%-6s%-20s%-15s%-40s%-20s%-20s\n";

	public BookingController() {
		scheduleDaoImpl = new ScheduleDAOImpl();
		cineplexDaoImpl = new CineplexDAOImpl();
		movieDaoImpl = new MovieDAOImpl();
		holidayDaoImpl = new HolidayDAOImpl();
	}

	/**
	 * Add a new booking record
	 * 
	 * @param movie
	 *            the movie to be added
	 */
	public void addBooking(Movie movie) {
		Booking booking = new Booking();
		List<Schedule> schedules = new ArrayList<Schedule>();
		List<Cineplex> cineplexes = cineplexDaoImpl.readAll();
		List<Movie> movies = movieDaoImpl.readActiveMovies();

		// Step 1: select movies
		Movie movieSelected;
		if (movie == null)
			movieSelected = InputMovie(movies);
		else
			movieSelected = movie;
		System.out.println("You have selected " + movieSelected.getTitle());
		for (int i = 0; i < movieSelected.getMovieType().length; i++) {
			double extraPrice = Movie.getAdditionalTicketPrice(movieSelected.getMovieType()[i]);
			if (extraPrice > 0)
				System.out.println("The movie is a " + movieSelected.getMovieType()[i] + " and there is a $"
						+ extraPrice + " extra charges.");
		}
		System.out.println();

		// Step 2: select cineplex
		Cineplex cineplexSelected;
		cineplexSelected = InputCineplex(cineplexes);
		System.out.println("You have selected " + cineplexSelected.getName());

		// Step 3: select schedule
		Schedule scheduleSelected;
		schedules = scheduleDaoImpl.readByCineplexAndMovie(cineplexSelected, movieSelected);
		if (schedules == null) {
			System.out.println("Sorry, there is no schedule available.");
			return;
		}
		scheduleSelected = InputSchedule(schedules);
		booking.setSchedule(scheduleSelected);

		System.out.println("You have chosen cinema of class " + scheduleSelected.getCinema().getCinemaClass()
				+ " and the base ticket price is $" + scheduleSelected.getCinema().getTicketPrice() + ".");

		// Step 4: input number of movie goers
		int totalMovieGoer = 1;
		int noOfSeatAvailable = calculateNoOfSeatAvailable(scheduleSelected);
		boolean isValid = false;
		while (!isValid) {
			System.out.println("How many movie goers?");
			totalMovieGoer = InputHelper.getInt();
			if (totalMovieGoer < 1)
				System.out.println("There must be at least 1 movie goer. Please input again.");
			else if (totalMovieGoer > noOfSeatAvailable)
				System.out.println("There are not enough available seats. Please input again.");
			else
				isValid = true;
		}

		Seat[] seats = new Seat[totalMovieGoer];
		String[][] tempSeatArrangement = scheduleSelected.getCinema().getSeatsAvailable();
		double price = 0;
		boolean isSeniorCitizenOrChild = false;
		int countSeniorCitizenOrChild = 0;

		for (int i = 0; i < totalMovieGoer; i++) {
			// Step 5: select seats
			OutputHelper.printSeatArrangement(tempSeatArrangement);
			System.out.println("Movie Goer " + (i + 1) + ": ");

			int row = 0;
			int col = 0;
			isValid = false;
			do {
				System.out.println("Select a row");
				row = InputHelper.getInt();
				System.out.println("Select a column");
				col = InputHelper.getInt();

				if (row > tempSeatArrangement.length || col > tempSeatArrangement[0].length || row < 1 || col < 1) {
					System.out.println("Your input is not valid. Please try again.");
					continue;
				}

				if (!tempSeatArrangement[row - 1][col - 1].equals("Taken")) {
					tempSeatArrangement[row - 1][col - 1] = "Taken";
					seats[i] = new Seat(row, col);
					isValid = true;
				} else {
					System.out.println("This seat has been taken. Please choose another one.");
				}
			} while (!isValid);

			// Step 6: check if the user a senior citizen

			System.out.println("Input the age of the movie goer");
			int age = InputHelper.getInt();
			if (age >= 55 || age <= 12) {
				isSeniorCitizenOrChild = true;
				countSeniorCitizenOrChild++;
			}
			// Step 7: Calculate gross ticket prices
			double additionalPrices = Movie.getTotalAdditionalTicketPrice(scheduleSelected.getMovie().getMovieType());

			price += scheduleSelected.getCinema().getTicketPrice() + additionalPrices
					- (isSeniorCitizenOrChild ? 2 : 0);
		}
		booking.setSeats(seats);

		// Step 8 in put customer details
		System.out.println("Input your name: ");
		String customerName = InputHelper.getString();

		System.out.println("Input your contact: ");
		String customerContact = InputHelper.getString();

		System.out.println("Input your email: ");
		String customerEmail = InputHelper.getString();

		booking.setCustomer(new Customer(customerName, customerContact, customerEmail));

		// Step 9: check if the showing day a public holiday or week
		boolean isOnHolidayOrWeekend = false;
		String holidayName = "";
		List<Holiday> holidays = holidayDaoImpl.readAll();
		for (int i = 0; i < holidays.size(); i++) {
			Holiday holiday = holidays.get(i);
			if ((holiday.getDate().get(Calendar.YEAR) == scheduleSelected.getShowingDateTime().get(Calendar.YEAR)
					&& holiday.getDate().get(Calendar.MONTH) == scheduleSelected.getShowingDateTime()
							.get(Calendar.MONTH)
					&& holiday.getDate().get(Calendar.DATE) == scheduleSelected.getShowingDateTime()
							.get(Calendar.DATE))) {
				isOnHolidayOrWeekend = true;
				holidayName = holiday.getName();
				System.out.println("The day is " + holidayName + " . Additional $2 will be added for each ticket.");
			} else if (isWeekend(scheduleSelected.getShowingDateTime())) {
				isOnHolidayOrWeekend = true;
				holidayName = "Weekend";
			}
		}

		if (isOnHolidayOrWeekend)
			price += 2 * totalMovieGoer;

		// Step 10: user make payment
		String priceBreakDownFormat = "%-35s%-5s%-5s%5s\n";

		// Line base price
		System.out.printf(priceBreakDownFormat, "Base Price", totalMovieGoer + " x", "$",
				OutputHelper.get2DecimalPlacesString(scheduleSelected.getCinema().getTicketPrice()));

		// Lines for movie type additional price
		for (int i = 0; i < movieSelected.getMovieType().length; i++) {
			double extraPrice = Movie.getAdditionalTicketPrice(movieSelected.getMovieType()[i]);
			if (extraPrice > 0)
				System.out.printf(priceBreakDownFormat, movieSelected.getMovieType()[i], totalMovieGoer + " x", "$",
						OutputHelper.get2DecimalPlacesString(extraPrice));
		}

		// Line holiday or weekend
		if (isOnHolidayOrWeekend)
			System.out.printf(priceBreakDownFormat, holidayName, totalMovieGoer + " x", "$",
					OutputHelper.get2DecimalPlacesString(2.0));

		// Line senior citizen or children
		if (isSeniorCitizenOrChild)
			System.out.printf(priceBreakDownFormat, "Elderly and Children Discount", countSeniorCitizenOrChild + " x",
					"$", "2.00");

		// Total Price
		System.out.println("----------------------------------------------------");
		System.out.printf(priceBreakDownFormat, "Total price: ", "", "$", OutputHelper.get2DecimalPlacesString(price));

		booking.setTransactionAmount(price);
		Calendar currentDateTime = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddkkmm");
		String transactionId = scheduleSelected.getCinema().getShortCode()
				+ dateFormat.format(currentDateTime.getTime());
		booking.setTransactionId(transactionId);

		// Step 11: generate booking record and update seat arrangement in the schedule
		scheduleSelected.getBookings().add(booking);
		scheduleSelected.getCinema().setSeatsAvailable(tempSeatArrangement);
		scheduleDaoImpl.update(scheduleSelected.getId(), scheduleSelected);

		// Step 12: notify user success of booking
		System.out.println("Your have successfully booked the movie! See you soon!");
		movieSelected.setNoOfTicketSold(movieSelected.getNoOfTicketSold() + totalMovieGoer);
		movieDaoImpl.update(movieSelected.getId(), movieSelected);

	}

	/**
	 * Check user's previous booking
	 */
	public void checkBookingHistory() {
		System.out.println("Input your email address to retrieve your booking history.");
		String email = InputHelper.getString();
		List<Booking> historyBookings = new ArrayList<Booking>();
		List<Schedule> schedules = scheduleDaoImpl.readAll();
		for (int i = 0; i < schedules.size(); i++) {
			List<Booking> bookings = schedules.get(i).getBookings();
			for (int j = 0; j < bookings.size(); j++) {
				if (bookings.get(j).getCustomer().getEmail().equals(email))
					historyBookings.add(bookings.get(j));
			}
		}

		if (historyBookings.size() == 0) {
			System.out.println(STR_BOOKING_NOT_FOUND);
			return;
		}

		OutputHelper.printBookingList(historyBookings);
	}

	/**
	 * input selection of cineplex
	 * 
	 * @param cineplexes
	 *            list of cineplexes
	 * @return the cineplex selected
	 */
	private static Cineplex InputCineplex(List<Cineplex> cineplexes) {
		int choice = 0;
		Cineplex cineplexSelected = null;
		boolean isSelected = false;

		OutputHelper.printCineplexList(cineplexes);

		do {
			System.out.println("Select a cineplex by input the index: ");
			choice = InputHelper.getInt();
			if (choice >= 1 && choice <= cineplexes.size()) {
				cineplexSelected = cineplexes.get(choice - 1);
				isSelected = true;
			} else {
				System.out.println(STR_CINEPLEX_NOT_FOUND);
			}

		} while (cineplexSelected == null);

		return cineplexSelected;
	}

	/**
	 * input selection of cinema
	 * 
	 * @param cinemas
	 *            list of cinemas
	 * @return the cinema selected
	 */
	private static Cinema InputCinema(List<Cinema> cinemas) {
		int choice = 0;
		Cinema cinemaSelected = null;
		boolean isSelected = false;

		OutputHelper.printCinemaList(cinemas);

		do {
			System.out.println("Select a cinema by input the index: ");
			choice = InputHelper.getInt();
			if (choice >= 1 && choice <= cinemas.size()) {
				cinemaSelected = cinemas.get(choice - 1);
				isSelected = true;
			} else {
				System.out.println(STR_CINEMA_NOT_FOUND);
			}
		} while (!isSelected);

		return cinemaSelected;
	}

	/**
	 * input selection of movie
	 * 
	 * @param movies
	 *            list of movies
	 * @return the movie selected
	 */
	private static Movie InputMovie(List<Movie> movies) {
		int choice = 0;
		Movie movieSelected = null;
		boolean isSelected = false;

		OutputHelper.printMovieList(movies, true);

		do {
			System.out.println("Select a movie by input the index: ");
			choice = InputHelper.getInt();
			if (choice >= 1 && choice <= movies.size()) {
				movieSelected = movies.get(choice - 1);
				isSelected = true;
			} else {
				System.out.println(STR_MOVIE_NOT_FOUND);
			}

		} while (movieSelected == null);

		return movieSelected;
	}

	/**
	 * input selection of movie
	 * 
	 * @param movies
	 *            list of movies
	 * @return the movie selected
	 */
	private static Schedule InputSchedule(List<Schedule> schedules) {
		int choice = 0;
		Schedule scheduleSelected = null;
		boolean isSelected = false;

		OutputHelper.printScheduleList(schedules, true);

		do {
			System.out.println("Select a schedule by input the index: ");
			choice = InputHelper.getInt();
			if (choice >= 1 && choice <= schedules.size()) {
				scheduleSelected = schedules.get(choice - 1);
				isSelected = true;
			} else {
				System.out.println(STR_SCHEDULE_NOT_FOUND);
			}

		} while (scheduleSelected == null);

		return scheduleSelected;
	}

	/**
	 * Calculate total number of available seat in a schedule
	 * 
	 * @param schedule
	 *            target schedue
	 * @return number of available seats
	 */
	private static int calculateNoOfSeatAvailable(Schedule schedule) {
		int count = 0;
		String[][] initialSeatArrangement = schedule.getCinema().getSeatsAvailable();
		for (int i = 0; i < initialSeatArrangement.length; i++)
			for (int j = 0; j < initialSeatArrangement[i].length; j++)
				if (initialSeatArrangement[i][j].equals("Available"))
					count++;

		return count;
	}

	/**
	 * Calculate total number of available seat in a schedule
	 * 
	 * @param date
	 *            Calendar date
	 * @return boolean value to determine if the date is on weekend
	 */
	private static boolean isWeekend(Calendar date) {
		boolean result = false;
		int day = date.get(Calendar.DAY_OF_WEEK);
		switch (date.get(Calendar.DAY_OF_WEEK)) {
		case 1:
		case 7:
			result = true;
			break;
		}
		return result;
	}
}
