package Control;

import java.util.Scanner;

import Dao.AdminDAOImpl;
import Entity.Admin;
import Helper.InputHelper;

public class AdminController {
	private AdminDAOImpl adminDaoImpl;
	private Admin admin;
	private boolean isLogin;

	public AdminController() {
		adminDaoImpl = new AdminDAOImpl();
		this.isLogin = false;
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public boolean isLogin() {
		return isLogin;
	}

	public void setLogin(boolean isLogin) {
		this.isLogin = isLogin;
	}

	/**
	 * handles user login function.
	 */
	public void login() {
		String username;
		String password;
		System.out.println("Username: ");
		username = InputHelper.getWord();
		System.out.println("Password: ");
		password = InputHelper.getWord();
		
		admin = adminDaoImpl.login(username, password);
		if (admin.getUsername() == null) {
			System.out.println("Your username or password is not correct. Please try again.");
			this.isLogin = false;
		} else {
			System.out.println("Login successfully.");
			this.isLogin = true;
		}

	}
	
	/**
	 * handles user logout function.
	 */
	public void logout() {
		this.admin = null;
		this.isLogin = false;
		System.out.println("Your logged out successfully.");
	}

//	public static void main(String args[]) {
//		AdminDAOImpl adminDaoImpl = new AdminDAOImpl();
//		Admin admin = new Admin();
//		admin.setId(1);
//		admin.setUsername("admin");
//		admin.setPassword("123");
//		adminDaoImpl.create(admin);
//	}
}
