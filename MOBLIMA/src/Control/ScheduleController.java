package Control;

import java.util.*;
import Dao.*;
import Entity.*;
import Helper.InputHelper;
import Helper.OutputHelper;

public class ScheduleController {
	private ScheduleDAOImpl scheduleDaoImpl;
	private CineplexDAOImpl cineplexDaoImpl;
	// private CinemaDAOImpl cinemaDaoImpl;
	private MovieDAOImpl movieDaoImpl;
	private final static String STR_SCHEDULE_NOT_FOUND = "Sorry, no schedule is found.";
	private final static String STR_CINEPLEX_NOT_FOUND = "Sorry, no cineplex is found.";
	private final static String STR_CINEMA_NOT_FOUND = "Sorry, no cinema is found.";
	private final static String STR_MOVIE_NOT_FOUND = "Sorry, no movie is found.";

	public ScheduleController() {
		scheduleDaoImpl = new ScheduleDAOImpl();
		cineplexDaoImpl = new CineplexDAOImpl();
		// cinemaDaoImpl = new CinemaDAOImpl();
		movieDaoImpl = new MovieDAOImpl();
	}

	/**
	 * List all the schedules stored in flat DB
	 */
	public void listAllSchedules() {
		List<Schedule> schedules = scheduleDaoImpl.readAll();
		System.out.println();
		if (schedules != null && schedules.size() > 0) {
			OutputHelper.printScheduleList(schedules, false);
		} else {
			System.out.println(STR_SCHEDULE_NOT_FOUND);
		}
	}

	/**
	 * Search specific schedule
	 */
	public void searchSchedule() {
		List<Schedule> schedules = new ArrayList<Schedule>();
		List<Cineplex> cineplexes = cineplexDaoImpl.readAll();
		List<Movie> movies = movieDaoImpl.readActiveMovies();

		Cineplex cineplexSelected;
		Movie movieSelected;

		cineplexSelected = InputCineplex(cineplexes);
		System.out.println("You have selected " + cineplexSelected.getName());

		movieSelected = InputMovie(movies);

		schedules = scheduleDaoImpl.readByCineplexAndMovie(cineplexSelected, movieSelected);

		if (schedules != null && schedules.size() > 0) {
			OutputHelper.printScheduleList(schedules, false);
		} else {
			System.out.println(STR_SCHEDULE_NOT_FOUND);
		}
	}

	/**
	 * add a new schedule to database
	 */
	public void addSchedule() {
		List<Cineplex> cineplexes = cineplexDaoImpl.readAll();
		List<Cinema> cinemas;
		List<Movie> movies = movieDaoImpl.readActiveMovies();
		Schedule schedule = new Schedule();
		Cineplex cineplexSelected;
		Cinema cinemaSelected;
		Movie movieSelected;
		Calendar showingDateTime;
		Calendar endDateTime;

		cineplexSelected = InputCineplex(cineplexes);
		System.out.println("You have selected " + cineplexSelected.getName());

		cinemaSelected = InputCinema(cineplexSelected.getCinemas());
		initializeSeatArrangement(cinemaSelected);

		System.out.println("You have selected " + cinemaSelected.getName());

		movieSelected = InputMovie(movies);

		boolean isShowingDateTimeValid = false;
		do {
			System.out.println("Showing Date Time: ");
			showingDateTime = InputHelper.getDateTime();
			endDateTime = OutputHelper.getEndTime(showingDateTime, movieSelected);

			if (showingDateTime.compareTo(movieSelected.getEndOfDate()) > 0) {
				System.out.println("The movie has ended. Please select another time.");
			} else {
				System.out.println(OutputHelper.getDateTimeDisplayString(showingDateTime) + " to "
						+ OutputHelper.getDateTimeDisplayString(endDateTime));
				isShowingDateTimeValid = true;
			}
		} while (!isShowingDateTimeValid);
		boolean result = scheduleDaoImpl.create(new Schedule(movieSelected, cinemaSelected, showingDateTime));
		if (result)
			System.out.println("Schedule is created successfully.");
		else
			System.out.println("Failed to create the schedule.");
	}

	/**
	 * remove a schedule from database
	 */
	public void removeSchedule() {
		System.out.println("Input schedule ID to remove: ");
		long id = InputHelper.getLong();
		boolean result = scheduleDaoImpl.delete(id);
		if (result)
			System.out.println("Deleted " + id + " Successfully");
		else
			System.out.println("Failed to delete schedule " + id);
	}

	/**
	 * input selection of cineplex
	 * 
	 * @param cineplexes
	 *            list of cineplexes
	 * @return the cineplex selected
	 */
	private static Cineplex InputCineplex(List<Cineplex> cineplexes) {
		int choice = 0;
		Cineplex cineplexSelected = null;
		boolean isSelected = false;

		OutputHelper.printCineplexList(cineplexes);

		do {
			System.out.println("Select a cineplex by input the index: ");
			choice = InputHelper.getInt();
			if (choice >= 1 && choice <= cineplexes.size()) {
				cineplexSelected = cineplexes.get(choice - 1);
				isSelected = true;
			} else {
				System.out.println(STR_CINEPLEX_NOT_FOUND);
			}

		} while (cineplexSelected == null);

		return cineplexSelected;
	}

	/**
	 * input selection of cinema
	 * 
	 * @param cinemas
	 *            list of cinemas
	 * @return the cinema selected
	 */
	private static Cinema InputCinema(List<Cinema> cinemas) {
		int choice = 0;
		Cinema cinemaSelected = null;
		boolean isSelected = false;

		OutputHelper.printCinemaList(cinemas);

		do {
			System.out.println("Select a cinema by input the index: ");
			choice = InputHelper.getInt();
			if (choice >= 1 && choice <= cinemas.size()) {
				cinemaSelected = cinemas.get(choice - 1);
				isSelected = true;
			} else {
				System.out.println(STR_CINEMA_NOT_FOUND);
			}
		} while (!isSelected);

		return cinemaSelected;
	}

	/**
	 * input selection of movie
	 * 
	 * @param movies
	 *            list of movies
	 * @return the movie selected
	 */
	private static Movie InputMovie(List<Movie> movies) {
		int choice = 0;
		Movie movieSelected = null;
		boolean isSelected = false;

		OutputHelper.printMovieList(movies, true);

		do {
			System.out.println("Select a movie by input the index: ");
			choice = InputHelper.getInt();
			if (choice >= 1 && choice <= movies.size()) {
				movieSelected = movies.get(choice - 1);
				isSelected = true;
			} else {
				System.out.println(STR_MOVIE_NOT_FOUND);
			}

		} while (movieSelected == null);

		return movieSelected;
	}

	/**
	 * Initialize seats to be available for booking
	 * 
	 * @param cinema
	 *            the target cinema which is the seats belong to
	 */
	private static void initializeSeatArrangement(Cinema cinema) {
		String[][] initialSeatArrangement = cinema.getSeatsAvailable();
		for (int i = 0; i < initialSeatArrangement.length; i++) {
			for (int j = 0; j < initialSeatArrangement[i].length; j++) {
				initialSeatArrangement[i][j] = "Available";
			}
		}
		cinema.setSeatsAvailable(initialSeatArrangement);
	}

}
