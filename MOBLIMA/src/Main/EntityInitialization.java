package Main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import Dao.*;
import Entity.*;

public class EntityInitialization {

	public static void main(String[] args) {
		CineplexDAOImpl cineplexDaoImpl = new CineplexDAOImpl();
		MovieDAOImpl movieDaoImpl = new MovieDAOImpl();
		HolidayDAOImpl holidayDaoImpl = new HolidayDAOImpl();
		ScheduleDAOImpl scheduleDaoImpl = new ScheduleDAOImpl();

		cineplexDaoImpl.deleteAll();
		movieDaoImpl.deleteAll();
		holidayDaoImpl.deleteAll();
		scheduleDaoImpl.deleteAll();

		try {
			// Initialize holidays
			Calendar holiday1 = Calendar.getInstance();
			holiday1.set(Calendar.YEAR, 2017);
			holiday1.set(Calendar.MONTH, 11);
			holiday1.set(Calendar.DATE, 25);
			holidayDaoImpl.create(new Holiday("Christmas Day", holiday1));
			TimeUnit.MILLISECONDS.sleep(1);
			Calendar holiday2 = Calendar.getInstance();
			holiday2.set(Calendar.YEAR, 2018);
			holiday2.set(Calendar.MONTH, 0);
			holiday2.set(Calendar.DATE, 1);
			holidayDaoImpl.create(new Holiday("New Year", holiday2));
			TimeUnit.MILLISECONDS.sleep(1);

			ArrayList<Cinema> cinemaList1 = new ArrayList<Cinema>();
			ArrayList<Cinema> cinemaList2 = new ArrayList<Cinema>();
			ArrayList<Cinema> cinemaList3 = new ArrayList<Cinema>();

			// Initialize cineplexes
			Cineplex gvjp = new Cineplex("GV Jurong Point", "GVJP",
					"1 Jurong West Central, #03-25B/26, Jurong Point, Singapore 648886", "66538100", cinemaList1);
			TimeUnit.MILLISECONDS.sleep(1);
			Cineplex gvpl = new Cineplex("GV Paya Lebar", "GVPL",
					"SingPost Centre, 10 Eunos Road 8, #03-107, Singapore 408600", "66538200", cinemaList2);
			TimeUnit.MILLISECONDS.sleep(1);
			Cineplex gvtp = new Cineplex("GV Tampines", "GVTP",
					"4 Tampines Central, #04-17/18, Tampines Mall Shopping Centre, Singapore 529510", "66538300",
					cinemaList3);

			// Initialize cinemas

			gvjp.getCinemas()
					.add(new Cinema("JP Gold Class", "JP1", Cinema.CinemaClass.PLATINUM, gvjp, new String[6][6], 36));
			gvjp.getCinemas()
					.add(new Cinema("JP Grand Seat", "JP2", Cinema.CinemaClass.GOLD, gvjp, new String[10][10], 100));
			gvjp.getCinemas()
					.add(new Cinema("JP Normal 1", "JP3", Cinema.CinemaClass.NORMAL, gvjp, new String[15][15], 225));
			gvjp.getCinemas()
					.add(new Cinema("JP Normal 2", "JP4", Cinema.CinemaClass.NORMAL, gvjp, new String[15][15], 225));

			gvpl.getCinemas()
					.add(new Cinema("PL Gold Class", "PL1", Cinema.CinemaClass.GOLD, gvpl, new String[6][6], 36));
			gvpl.getCinemas()
					.add(new Cinema("PL Grand Seat", "PL2", Cinema.CinemaClass.SILVER, gvpl, new String[10][10], 100));
			gvpl.getCinemas()
					.add(new Cinema("PL Normal 1", "PL3", Cinema.CinemaClass.NORMAL, gvpl, new String[15][15], 225));
			gvpl.getCinemas()
					.add(new Cinema("PL Normal 2", "PL4", Cinema.CinemaClass.NORMAL, gvpl, new String[15][15], 225));

			gvtp.getCinemas()
					.add(new Cinema("TP Gold Class", "TP1", Cinema.CinemaClass.GOLD, gvtp, new String[6][6], 36));
			gvtp.getCinemas()
					.add(new Cinema("TP Grand Seat", "TP2", Cinema.CinemaClass.SILVER, gvtp, new String[10][10], 100));
			gvtp.getCinemas()
					.add(new Cinema("TP Normal 1", "TP3", Cinema.CinemaClass.NORMAL, gvtp, new String[15][15], 225));
			gvtp.getCinemas()
					.add(new Cinema("TP Normal 2", "TP4", Cinema.CinemaClass.NORMAL, gvtp, new String[15][15], 225));

			cineplexDaoImpl.create(gvjp);
			cineplexDaoImpl.create(gvpl);
			cineplexDaoImpl.create(gvtp);

			// Initialize movies
			String[] casts1 = { "Colin Firth", "Julianne Moore", "Taron Egerton", "Mark Strong", "Halle Berry",
					"Sir Elton John", "Channing Tatum", "Jeff Bridges" };
			Calendar date1 = Calendar.getInstance();
			movieDaoImpl.create(new Movie("Kingsman: The Golden Circle", "Coming Soon", "R21",
					"\"Kingsman: The Secret Service\" introduced the world to Kingsman - an independent, international intelligence agency operating at the highest level of discretion, whose ultimate goal is to keep the world safe. In \"Kingsman: The Golden Circle,\" our heroes face a new challenge. When their headquarters are destroyed and the world is held hostage, their journey leads them to the discovery of an allied spy organization in the US called Statesman, dating back to the day they were both founded. In a new adventure that tests their agents' strength and wits to the limit, these two elite secret organizations band together to defeat a ruthless common enemy, in order to save the world, something that's becoming a bit of a habit for Eggsy... ",
					"Matthew Vaughn", new Movie.MovieType[] { Movie.MovieType.ACTION, Movie.MovieType.THREE_D,
							Movie.MovieType.BLOCKBUSTER },
					142, casts1, date1, new ArrayList<Feedback>(), 0));
			TimeUnit.MILLISECONDS.sleep(1);

			String[] casts2 = { "Gerard Butler", "Jim Sturgess", "Abbie Cornish", "Daniel Wu", "Ed Harris",
					"Andy Garcia" };
			Calendar date2 = Calendar.getInstance();
			movieDaoImpl.create(new Movie("Geostorm", "Preview", "PG13",
					"After an unprecedented series of natural disasters threatened the planet, the world°Æs leaders came together to create an intricate network of satellites to control the global climate and keep everyone safe. But now, something has gone wrong°Æthe system built to protect the Earth is attacking it, and it°Æs a race against the clock to uncover the real threat before a worldwide geostorm wipes out everything... and everyone along with it. ",
					"Dean Devlin", new Movie.MovieType[] { Movie.MovieType.HORROR, Movie.MovieType.SCIFI }, 101, casts2,
					date2, new ArrayList<Feedback>(), 0));
			TimeUnit.MILLISECONDS.sleep(1);

			String[] casts3 = { "Dave Franco", "Justin Theroux", "Fred Armisen", "Abbi Jacobson", "Olivia Munn" };
			Calendar date3 = Calendar.getInstance();
			movieDaoImpl.create(new Movie("The Lego Ninjago Movie", "Now Showing", "PG",
					"In this big-screen Ninjago adventure, the battle for Ninjago City calls to action young Master Builder Lloyd, aka the Green Ninja, along with his friends, who are all secret ninja warriors. Led by Master Wu, as wise-cracking as he is wise, they must defeat evil warlord Garmadon, The Worst Guy Ever, who also happens to be Lloyd's dad. Pitting mech against mech and father against son, the epic showdown will test this fierce but undisciplined team of modern-day ninjas who must learn to check their egos and pull together to unleash their inner power of Spinjitzu. ",
					"Charlie Bean", new Movie.MovieType[] { Movie.MovieType.COMEDY, Movie.MovieType.THREE_D }, 109,
					casts3, date3, new ArrayList<Feedback>(), 0));
			TimeUnit.MILLISECONDS.sleep(1);

			String[] casts4 = { "Chris Hemsworth", "Tom Hiddleston", "Cate Blanchett", "Idris Elba", "Jeff Goldblum",
					"Tessa Thompson", "Karl Urban", "Mark Ruffalo", "Anthony Hopkins" };
			Calendar date4 = Calendar.getInstance();
			movieDaoImpl.create(new Movie("Marvel's Thor: Ragnarok", "Now Showing", "PG13",
					"In Marvel Studios' \"Thor: Ragnarok\", Thor is imprisoned on the other side of the universe without his mighty hammer and finds himself in a race against time to get back to Asgard to stop Ragnarok- the destruction of his homeworld and the end of Asgardian civilization- the hands of an all-powerful new threat, the ruthless Hela. But first he must survive a deadly gladiatorial contest that pits him against his former ally and fellow Avenger- the Incredible Hulk!",
					"Taika Waititi", new Movie.MovieType[] { Movie.MovieType.ACTION, Movie.MovieType.THREE_D }, 130,
					casts4, date4, new ArrayList<Feedback>(), 0));
			TimeUnit.MILLISECONDS.sleep(1);
			
			String[] casts5 = { 
					"Kate Winslet", "Idris Elba", "Beau Bridges", "Dermot Mulroney" };
			Calendar date5 = Calendar.getInstance();
			movieDaoImpl.create(new Movie("The Mountain Between Us", "Coming Soon", "M18",
					"Stranded after a tragic plane crash, two strangers must forge a connection to survive the extreme elements of a remote snow covered mountain. When they realize help is not coming, they embark on a perilous journey across hundreds of miles of wilderness, pushing one another to endure and discovering strength they never knew possible. The film is directed by Academy Award nominee Hany Abu-Assad and stars Academy Award winner Kate Winslet and Golden Globe winner Idris Elba. ",
					"Hany Abu-Assad", new Movie.MovieType[] { Movie.MovieType.THREE_D }, 112,
					casts5, date5, new ArrayList<Feedback>(), 0));
			TimeUnit.MILLISECONDS.sleep(1);

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Initialization done.");
	}

}
