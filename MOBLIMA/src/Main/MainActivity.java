package Main;

import java.util.Scanner;

import Control.*;
import Dao.*;
import Entity.*;
import Helper.*;

public class MainActivity {

	private static AdminController adminCtrl;
	private static MovieController movieCtrl;
	private static ScheduleController scheduleCtrl;
	private static BookingController bookingCtrl;
	private static HolidayController holidayCtrl;
	
	

	/**
	 * Initialization of the console application.
	 * 
	 * @param args console arguments
	 */
	public static void main(String[] args) {
		adminCtrl = new AdminController();
		movieCtrl = new MovieController();
		scheduleCtrl = new ScheduleController();
		bookingCtrl = new BookingController();
		holidayCtrl = new HolidayController();

		System.out.println("Welcome to MOBLIMA. Please type respective number to proceed.");
		mainMenu();
		System.out.println("Exit app successfully. Bye.");
	}

	/**
	 * Main menu display.
	 */
	public static void mainMenu() {
		int choice = 0;
		do {
			if (!adminCtrl.isLogin()) {
				System.out.println("1. List all movies");
				System.out.println("2. Search a movie");
				System.out.println("3. View a movie detail");
				System.out.println("4. Book a movie");
				System.out.println("5. Rate and review a movie");
				System.out.println("6. Check booking history");
				//Add new choice
				System.out.println("7. List all holidays");
				System.out.println("9. Admin Login");
				System.out.println("0. Exit");

				choice = InputHelper.getInt();

				switch (choice) {
				case 1:
					movieCtrl.listAllMovies();
					break;
				case 2:
					movieCtrl.searchMovie();
					break;
				case 3:
					movieCtrl.viewMovie();
					break;
				case 4:
					bookingCtrl.addBooking(null);
					break;
				case 5:
					movieCtrl.feedbackMovie();
					break;
				case 6:
					bookingCtrl.checkBookingHistory();
					break;
				
				// Add case list all holidays
				case 7:
					holidayCtrl.listAllHolidays();
					break;
				case 9:
					adminCtrl.login();
					break;
				case 0:
					break;
				default:
					System.out.println("Sorry, your input is not valid. Please try again.");
				}

			} else {
				System.out.println("1. Manage Movies");
				System.out.println("2. Manage Movie Schedules");
				System.out.println("3. Holiday Configuration");
				System.out.println("4. View movie rankings");
				System.out.println("9. Logout");
				System.out.println("0. Exit");

				choice = InputHelper.getInt();

				switch (choice) {
				case 1:
					movieManagementMenu();
					break;
				case 2:
					scheduleManagementMenu();
					break;
				case 3:
					holidayManagementMenu();
					break;
				case 4:
					movieRankMenu();
					break;
				case 9:
					adminCtrl.logout();
					break;
				case 0:
					break;
				default:
					System.out.println("Sorry, your input is not valid. Please try again.");
				}
			}
			System.out.println();
		} while (choice != 0);
	}

	/**
	 * Movie management menu display.
	 */
	public static void movieManagementMenu() {
		int choice = 0;
		do {
			System.out.println("1. List all movies");
			System.out.println("2. Search a movie");
			System.out.println("3. View a movie detail");
			System.out.println("4. Add a new movie");
			System.out.println("5. Update a movie");
			System.out.println("6. Remove a movie");
			System.out.println("9. Back");

			choice = InputHelper.getInt();

			switch (choice) {
			case 1:
				movieCtrl.listAllMovies();
				break;
			case 2:
				movieCtrl.searchMovie();
				break;
			case 3:
				movieCtrl.viewMovie();
				break;
			case 4:
				movieCtrl.addMovie();
				break;
			case 5:
				movieCtrl.updateMovie();
				break;
			case 6:
				movieCtrl.removeMovie();
				break;
			case 9:
				break;
			default:
				System.out.println("Sorry, your input is not valid. Please try again.");
			}
			System.out.println();
		} while (choice != 9);
	}

	/**
	 * Schedule management menu display.
	 */
	public static void scheduleManagementMenu() {
		int choice = 0;
		do {
			System.out.println("1. List all schedules");
			System.out.println("2. Search a schedule");
			System.out.println("3. Add a new schedule");
			System.out.println("4. Remove a schedule");
			System.out.println("9. Back");

			choice = InputHelper.getInt();

			switch (choice) {
			case 1:
				scheduleCtrl.listAllSchedules();
				break;
			case 2:
				scheduleCtrl.searchSchedule();
				break;
			case 3:
				scheduleCtrl.addSchedule();
				break;
			case 4:
				scheduleCtrl.removeSchedule();
				break;
			case 9:
				break;
			default:
				System.out.println("Sorry, your input is not valid. Please try again.");
			}
			System.out.println();
		} while (choice != 9);
	}
	
	/**
	 * Movie rank menu display.
	 */
	public static void movieRankMenu() {
		int choice = 0;
		do {
			System.out.println("1. Rank by Ticket Sales");
			System.out.println("2. Rank by Ratings");
			System.out.println("9. Back");

			choice = InputHelper.getInt();

			switch (choice) {
			case 1:
				movieCtrl.listRankedMoviesByTicketSold();
				break;
			case 2:
				movieCtrl.listRankedMoviesByRating();
				break;
			case 9:
				break;
			default:
				System.out.println("Sorry, your input is not valid. Please try again.");
			}
			System.out.println();
		} while (choice != 9);
	}
	//Add holiday management menu display
	/**
	 * Holiday management menu display.
	 */
	public static void holidayManagementMenu() {
		int choice = 0;
		do {
			System.out.println("1. List all holidays");	
			System.out.println("2. Add a new holiday");
			System.out.println("3. Update a holiday");
			System.out.println("4. Remove a holiday");
			System.out.println("9. Back");

			choice = InputHelper.getInt();

			switch (choice) {
			case 1:
				holidayCtrl.listAllHolidays();
				break;
			case 2:
				holidayCtrl.addHoliday();
				break;
			case 3:
				holidayCtrl.updateHoliday();
				break;
			case 4:
				holidayCtrl.removeHoliday();
				break;
			case 9:
				break;
			default:
				System.out.println("Sorry, your input is not valid. Please try again.");
			}
			System.out.println();
		} while (choice != 9);
	}
}
